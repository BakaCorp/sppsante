<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'sppsante');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=l(Qu>+oLhY0(AV@H4&uULK[6f3@H,WIJNF-03|Du%v_*tG.yoL,d(5tU*$7T?p;');
define('SECURE_AUTH_KEY',  'FCq<:{`v7(/S;(6Mkxcp^p&?j[A1~brV]Es^MKQ?rCN71zSFuL U/oWvUh@T~NI3');
define('LOGGED_IN_KEY',    'ssv,x*o Q*xcO_rxCwhN*xwZ/t -OXZ(eQ;k.% NiOJ2}g#9TXs9VswKO]qZ<oni');
define('NONCE_KEY',        '1mog.1fr^+T$lnqjNFIl;8fKQcmFJ46Ue=nzM ?>*!WKfq~GCjx]C;L>MwWbU6@S');
define('AUTH_SALT',        'UL#%=,2[GcU[qa@iqNrn@^|+skz([[K?-18{68xuq+E(#|5:)aK-j*.`[BuKT(Q8');
define('SECURE_AUTH_SALT', '{:uB}M+:FM&o!V0!}Y67tOWOZZEPW`b};MHH15)u=8A0M1+DLk):G0&M%uB>VoAe');
define('LOGGED_IN_SALT',   '-8D^S4d1`EwgE7W&_)gOSusw|vr<4-h!}^;)-:?7QA!b?/z^+Y@tOmk*N`BxO(xl');
define('NONCE_SALT',       'xg1m)hhOETT-(6s*hg#hXfSxg0uE~}*JXN/R~)ZM(nY^{2qGuUDy~1Mc)-~Uq]T-');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'spp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');