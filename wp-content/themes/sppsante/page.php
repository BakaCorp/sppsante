<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles;
?>

<?php while (have_posts()) : the_post(); ?>

  <div class="wrap pb-100 mb-lg-100" role="document">
    <div class="row">
      
      <?php if ( is_page() && $post->post_parent ) : ?>

        <div class="sidebar-menu d-none d-xl-flex col-xl-3 justify-content-center align-items-start">
          <div><?php include Wrapper\sidebar_path(); ?></div>
        </div><!-- /.sidebar -->

        <div class="page-content col-lg-12 col-xl-9">
          <?php get_template_part('templates/page', 'header'); ?>
          <?php get_template_part('templates/content', 'page'); ?>
        </div><!-- /.main -->

      <?php else : ?>

        <div class="page-content col-12">
          <?php get_template_part('templates/page', 'header'); ?>
          <?php get_template_part('templates/content', 'page'); ?>
        </div><!-- /.main -->

      <?php endif; ?>
      
    </div><!-- /.content -->
  </div><!-- /.wrap -->

<?php endwhile; ?>