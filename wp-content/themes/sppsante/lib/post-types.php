<?php

namespace Roots\Sage\PostTypes;

use Roots\Sage\Setup;

/**
 * Theme post-types
 */
function create_post_types(){
  register_post_type('communiques', array(
    'labels' => array(
      'name'                => 'Communiqués',
      'singular_name'       => 'Communiqué',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'communiques' ),
    'supports'      => array( 'title', 'editor', 'thumbnail'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-megaphone'
  ));

  register_post_type('packages', array(
    'labels' => array(
      'name'                => 'Packages',
      'singular_name'       => 'Package',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'packages' ),
    'supports'      => array( 'title', 'editor', 'page-attributes'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-awards'
  ));

  register_post_type('intervenants', array(
    'labels' => array(
      'name'                => 'Intervenants',
      'singular_name'       => 'Intervenant',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'intervenants' ),
    'supports'      => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
    'taxonomies' => array('category'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-businessman'
  ));

  register_post_type('programmes', array(
    'labels' => array(
      'name'                => 'Programmes',
      'singular_name'       => 'Programme',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'programmes' ),
    'supports'      => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
    'taxonomies' => array('category'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-calendar-alt'
  ));

  register_post_type('exposants', array(
    'labels' => array(
      'name'                => 'Exposants',
      'singular_name'       => 'Exposant',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'exposants' ),
    'supports'      => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
    'taxonomies' => array('category'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-store'
  ));

  register_post_type('partenaires', array(
    'labels' => array(
      'name'                => 'Partenaires',
      'singular_name'       => 'Partenaire',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'partenaires' ),
    'supports'      => array( 'title', 'editor', 'thumbnail'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-thumbs-up'
  ));

  register_post_type('faq-exposer', array(
    'labels' => array(
      'name'                => 'FAQ Exposer',
      'singular_name'       => 'FAQ Exposer',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'faq-exposer' ),
    'supports'      => array( 'title', 'editor'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-editor-help'
  ));

  register_post_type('faq-visiter', array(
    'labels' => array(
      'name'                => 'FAQ Visiter',
      'singular_name'       => 'FAQ Visiter',
      'add_new'             => 'Ajouter',
      'add_new_item'        => 'Ajouter',
      'edit'                => 'Éditer',
      'edit_item'           => 'Éditer',
      'new_item'            => 'Nouveau',
      'view'                => 'Afficher',
      'view_item'           => 'Afficher',
      'search_items'        => 'Rechercher',
      'not_found'           => 'Pas de résultats',
      'not_found_in_trash'  => 'Pas de résultats',
    ),
    'public'        => true,
    'hierarchical'  => true,
    'has_archive'   => false,
    'rewrite'       => array( 'slug' => 'faq-visiter' ),
    'supports'      => array( 'title', 'editor'),
    'can_export'    => true,
    'menu_icon'     => 'dashicons-editor-help'
  ));

  flush_rewrite_rules();
}
add_action( 'init', __NAMESPACE__ . '\\create_post_types');
