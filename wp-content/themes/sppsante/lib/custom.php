<?php

namespace Roots\Sage\Custom;

use Roots\Sage\Setup;

/**
 * Disable WordPress Admin Bar for all users
 */
show_admin_bar(false);

/**
 * Add Image sizes
 */
add_image_size( 'logo', 150, 150, false );
add_image_size( 'square', 350, 350, true );
add_image_size( 'page-header-xl', 1200, 300, true );
add_image_size( 'page-header-sm', 768, 180, true );
add_image_size( 'page-header-xs', 375, 88, true );
add_image_size( 'page-slider-xl', 1200, 500, true );
add_image_size( 'page-slider-sm', 768, 500, true );
add_image_size( 'page-slider-xs', 375, 240, true );

/**
* Join posts and postmeta tables
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
*/
function cf_search_join( $join ) {
  global $wpdb;
  $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  return $join;
}
add_filter('posts_join',  __NAMESPACE__ . '\\cf_search_join' );

/**
* Modify the search query with posts_where
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
*/
function cf_search_where( $where ) {
  global $pagenow, $wpdb;
  $where = preg_replace(
    "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
    "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
  return $where;
}
add_filter( 'posts_where',  __NAMESPACE__ . '\\cf_search_where' );

/**
* Prevent duplicates
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function cf_search_distinct( $where ) {
  global $wpdb;
  return "DISTINCT";
  return $where;
}
add_filter( 'posts_distinct',  __NAMESPACE__ . '\\cf_search_distinct' );

/**
* Exclude from search
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function removeFromSearch( $query ) {
  
  $query->set( 'post__not_in', array(1,2,3,4));

}
//add_action( 'pre_get_posts', __NAMESPACE__ . '\\removeFromSearch' );

/**
 * ADD ACF OPTIONS PAGE
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/**
* Embed Videos with container
*/
function wrap_embed_with_div($html, $url, $attr) {
	return "<div class=\"video-container\">".$html."</div>";
}
add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);


/**
 * Own SVG in ACF Field
 */
function bea_svg_icon_filepath( $filepath ) {
  if ( is_file( get_stylesheet_directory() . '/templates/content-svg.php' ) ) {
    $filepath = get_stylesheet_directory() . '/templates/content-svg.php';
  }
  return $filepath;
}
add_filter( 'acf_svg_icon_filepath', __NAMESPACE__ . '\\bea_svg_icon_filepath' );


/**
 * Limit excerpt to a number of characters
 * 
 * @param string $excerpt
 * @return string
 */
function custom_short_excerpt($excerpt){
	$limit = 75;

	if (strlen($excerpt) > $limit) {
		return substr($excerpt, 0, strpos($excerpt, ' ', $limit)).'...';
	}

	echo $excerpt;
}
add_filter('the_excerpt', __NAMESPACE__ . '\\custom_short_excerpt');

function custom_get_short_excerpt($excerpt){
	$limit = 75;

	if (strlen($excerpt) > $limit) {
		return substr($excerpt, 0, strpos($excerpt, ' ', $limit)).'...';
	}

	return $excerpt;
}
add_filter('get_the_excerpt', __NAMESPACE__ . '\\custom_get_short_excerpt');


/**
 * Only One product
 **/
function woo_custom_add_to_cart( $cart_item_data ) {

    global $woocommerce;
    $woocommerce->cart->empty_cart();

    // Do nothing with the data and return
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', __NAMESPACE__ . '\\woo_custom_add_to_cart' );


/**
 * Add checkbox field to the checkout
 **/
function my_custom_checkout_field( $checkout ) {

    $checked = $checkout->get_value( 'interets' ) ? $checkout->get_value( 'interets' ) : 1;

    echo '<div id="billing_interets"><h3>'.__('Cochez vos centres d\'intérêt').'</h3>';

    $categories = get_categories();
    foreach($categories as $index => $category) {
      if($category->term_id != 1) {
        woocommerce_form_field( 'interets['.$index.']', array(
          'type'          => 'checkbox',
          'class'         => array('input-checkbox'),
          'label'         => $category->name,
          'required'  => false,
        ), 0);
      }
    }

    echo '</div>';
}
add_action('woocommerce_after_order_notes', __NAMESPACE__ . '\\my_custom_checkout_field');




function wc_new_order_column( $columns ) {
  $columns['spp_column'] = 'Catégorie';
  return $columns;
}
add_filter( 'manage_edit-shop_order_columns', __NAMESPACE__ . '\\wc_new_order_column' );

function cw_add_category_column_content( $column ) {
  global $post;

  if ( 'spp_column' === $column ) {
    $order    = wc_get_order( $post->ID );
    $cat_in_order = false;
   
    $items = $order->get_items();
    foreach ( $items as $item ) {
      $product_id = $item['product_id'];
      if ( has_term( 'packages', 'product_cat', $product_id ) ) {
        $cat_in_order = true;
        break;
      }
    }

    if ( $cat_in_order ) {
      echo 'Stand';
    } else {
      echo 'Pass';
    }
  }
}
add_action( 'manage_shop_order_posts_custom_column', __NAMESPACE__ . '\\cw_add_category_column_content' );

function cw_add_category_column_style() {
  $css = '.widefat .column-spp_column { text-align: right; }';
  wp_add_inline_style( 'woocommerce_admin_styles', $css );
}
add_action( 'admin_print_styles', __NAMESPACE__ . '\\cw_add_category_column_style' );
