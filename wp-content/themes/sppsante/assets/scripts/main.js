/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // VAR
  var templatedirectory = $('#data-templatedirectory').data('templatedirectory');

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {

        //SCROLL TOP
        $('.scrollTop').on('click',function(){
          $('body,html').animate({
            scrollTop: 0
          }, 600);
          return false;
        });

        //LOGIN FORM
        placeholder_login = $('form.wc-auth-login label[for="username"]').text();
        placeholder_password = $('form.wc-auth-login label[for="password"]').text();
        $('form.wc-auth-login input[type="text"]').attr('placeholder', placeholder_login);
        $('form.wc-auth-login input[type="password"]').attr('placeholder', placeholder_password);        
        $('form.wc-auth-login label').remove();

        //LINK ACTIVE
        $('[data-active!=""]').on('click',function(){
          var domActive = $(this).data('active');
          $(domActive).toggleClass('active');
        });

        //MINIFY MENU
        var sideBarPaddingMax = $('.sidebar-menu').height() - $('.sidebar-menu > div').height();
        $(window).on('scroll', function(){
          var scrollTop = $(window).scrollTop();
          $('body').toggleClass('minified', scrollTop > 200);
          if(scrollTop < sideBarPaddingMax) { $('.sidebar-menu').css('paddingTop',scrollTop); }
        });

        //INACTIVE LINK
        $('a.inactive, .inactive > a').on('click',function(){
          $('#alert-inactive').addClass('show');
          return false;
        });
        $('.alert-close').on('click', function(){
          $(this).parent('.alert').removeClass('show');
        });
        $('.link-indications:not(.inactive)').on('click',function(){
          $('.link-indications').removeClass('active');
          $(this).addClass('active');
          $('.indication-content').hide(0, function(){ 
            $('.indication-content').removeClass('active');
          });
          var target = $(this).attr('href');
          $(target).show(0, function(){ 
            $(target).addClass('active');
          });
          return false;
        });
        $('.link-days').on('click',function(){
          $('.link-days,.day-programme').removeClass('active');
          $(this).addClass('active');
          var target = '.'+$(this).attr('href').substr(1);
          $(target).addClass('active');
        });

        var isSliding = false;
        $('.active-sidebar').on('click', function(){
          if (isSliding === true) {
            return false;
          }
          $('html').addClass('open-sidebar');
          $('.sidebar-persona').removeClass('active');
          $($(this).attr('href')).addClass('active');
          return false;
        });
        $('.sidebar-persona-close,.back-sidebar').on('click',function(){
          $('html').removeClass('open-sidebar');
          $('.sidebar-persona').removeClass('active');
          return false;
        });

        //DESACTIVE NOT CHILDREN MENU
        $('#menu-menu > li.menu-item-has-children > a').on('click',function(){
          if($(this).next('ul').length) {
            $('.sub-menu').removeClass('active');
            $(this).next('ul').addClass('active');
          }
          return false;
        });

        // AOS init
        AOS.init({
          //disable: 'mobile',
          duration: 600,
        });

        //SLIDER Orateurs
        $('.slider-orateurs').slick({
          autoplay: false,
          dots: false,
          infinite: false,
          arrows: false,
          speed: 500,
          slidesToShow: 3,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        });
        $('.slider-orateurs').on('setPosition',function(slick){
          AOS.refresh(true);
        });
        $('.slider-orateurs').on('beforeChange', function() {
          isSliding = true;
        });        
        $('.slider-orateurs').on('afterChange', function() {
          isSliding = false;
        });

        //SLIDER Actualites
        $('.slider-actualites').slick({
          autoplay: false,
          dots: false,
          infinite: false,
          arrows: false,
          speed: 500,
          slidesToShow: 2,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        });
        $('.slider-actualites').on('setPosition',function(slick){
          AOS.refresh(true);
        });

        // SLIDER Homepage
        var sliderActive = 0;
        var sliderMax = $('.slider-slide').length - 1;

        function slide() {
          $('.slider-circle').remove();
          $('.slider-slide').removeClass('active');
          $('.slider-content').find('.slider-slide').eq( sliderActive ).addClass('active');
          $('.slider-next').prepend('<svg class="slider-circle"><circle r="30" cx="32" cy="32"></circle></svg>');          
        }

        $('#slider-homepage .slider-next').on('click',function(){
          if(sliderActive < sliderMax) { sliderActive = sliderActive + 1; } else { sliderActive = 0; }
          slide();
        });
        $('#slider-homepage .slider-prev').on('click',function(){
          if(sliderActive > 0) { sliderActive = sliderActive - 1; } else { sliderActive = sliderMax; }
          slide();
        });
        $('body').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', '#slider-homepage .slider-circle circle', function () {
          if(sliderActive < sliderMax) { sliderActive = sliderActive + 1; } else { sliderActive = 0; }
          slide();
        });

        // SLIDER Pages
        var pageSliderActive = 0;
        var pageSliderMax = $('.page-slide').length - 1;

        function pageSlide() {
          $('.slider-circle').remove();
          $('.page-slide').removeClass('active');
          $('.page-slider').find('.page-slide').eq( pageSliderActive ).addClass('active');
          $('.slider-next').prepend('<svg class="slider-circle"><circle r="30" cx="32" cy="32"></circle></svg>');          
        }

        $('.page-slider .slider-next').on('click',function(){
          if(pageSliderActive < pageSliderMax) { pageSliderActive = pageSliderActive + 1; } else { pageSliderActive = 0; }
          pageSlide();
        });
        $('body').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', '.page-slider .slider-circle circle', function () {
          if(pageSliderActive < pageSliderMax) { pageSliderActive = pageSliderActive + 1; } else { pageSliderActive = 0; }
          pageSlide();
        });

      },
      finalize: function() {

      }
    },
    // Home page
    'home': {
      init: function() {

        //logotypespp animation
        var animation = bodymovin.loadAnimation({
          container: document.getElementById('logotypespp'),
          renderer: 'svg',
          loop: false,
          autoplay: true,
          path: templatedirectory + '/dist/custom/logotypespp.json'
        });
        
      },
      finalize: function() {
        
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
