<?php
/**
 * Template Name: Vente Template
 */
?>

<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles;
?>

<?php while (have_posts()) : the_post(); ?>

  <div class="wrap py-50 mb-lg-100" role="document">
    <div class="row">

      <div class="page-content col-12">
        <div class="page-header">
          
          <div class="col-12 col-xl-10 mx-xl-auto">
            <?php the_content(); ?>
          </div>
          
        </div>

        <?php get_template_part('templates/content', 'page'); ?>
      </div><!-- /.main -->
      
    </div><!-- /.content -->
  </div><!-- /.wrap -->

<?php endwhile; ?>