<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<div id="slider-homepage" class="d-lg-flex justify-content-lg-between align-items-center" data-aos="fade-right" data-aos-delay="800">
  <a class="slider-prev d-flex justify-content-center align-items-center color-white">
    <svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-right"></use></svg>
  </a>
  <a class="slider-next d-flex justify-content-center align-items-center color-white">
    <svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-left"></use></svg>
    <svg class="slider-circle"><circle r="31" cx="32" cy="31"></circle></svg>
  </a>
  <div class="slider-content">
    <div class="slider-slide d-flex align-items-center active" data-slide="1" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/recherche.png);">
      <div>
        <div class="fade-left">REC-</div>
        <div class="fade-left">HER-</div>
        <div class="fade-left">CHE</div>
      </div>
    </div>
    <div class="slider-slide d-flex align-items-center" data-slide="2" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/innovation.png);">
      <div>
        <div class="fade-left">INN-</div>
        <div class="fade-left">OVA-</div>
        <div class="fade-left">TION</div>
      </div>
    </div>
  </div>
  <div class="slider-slogan">
    <h2 class="color-white" data-aos="fade-left" data-aos-delay="1600">Le 1er salon-<br>conférences<br>dédié à la<br>prévention<br>en santé</h2>
    <h3 class="color-rose" data-aos="fade-left" data-aos-delay="1900">révervé aux<br>professionnels</h3>
  </div>
  <video autoplay loop muted playsinline>
    <source src="<?php echo get_template_directory_uri(); ?>/dist/custom/adn.mp4" type="video/mp4">
  </video>
</div>


<div class="container-fluid py-50 py-lg-100 background-filigrane">
  <div class="container">
    <div class="row align-items-center">

      <div class="d-none d-sm-block background-white col-sm-5 col-lg-6" data-aos="fade-right">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/homepage.png" alt="" class="w-100">
      </div>

      <div class="col-12 col-sm-6 col-lg-5 offset-sm-1 homepage-prevention" data-aos="fade-left">
        <h2 class="color-violet"><span class="color-rose">Investir dans la prévention</span><br>
        et l’innovation pour rélever<br>
        les défis économiques<br>
        et sociétaux de la santé<br>
        de demain</h2>
        <p class="color-violet mt-30 mt-lg-50">Le salon SPP Santé est l’occasion unique de réunir pendant 2 jours et en un même lieu tous les professionnels du secteur de la santé. Les différents acteurs, qu’ils soient médicaux, paramédicaux ou responsables institutionnels, tous concernés par l’urgence d’une prévention efficiente, efficace, rencontreront les différentes entreprises impliquées sur ce marché. En forte croissance, celui-ci se caractérise par une grande diversité d’acteurs.</p>
        <a href="#" class="mt-15 mt-lg-30 btn">En savoir plus sur le salon</a>
      </div>

    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.container-fluid -->


<div class="container-fluid background-gray-light py-50 py-lg-100">
  <div class="container">
    <div class="row">
      
      <div class="col-12 titre-decale">
        <h1 class="color-violet" data-aos="fade-down">Les orateurs</h1>
        <div class="slider-orateurs" data-aos="fade-up">
          <a href="#" class="d-block persona">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/orateur.png" alt="" class="w-100">
            <h3 class="mt-15 mt-lg-30">Stewart Butterfield</h3>
            <p>Co-fondateur et CEO<br>FitBit</p>
          </a>
          <a href="#" class="d-block persona">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/orateur.png" alt="" class="w-100">
            <h3 class="mt-15 mt-lg-30 color-rose">Stewart Butterfield</h3>
            <p>Co-fondateur et CEO<br>FitBit</p>
          </a>
          <a href="#" class="d-block persona">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/orateur.png" alt="" class="w-100">
            <h3 class="mt-15 mt-lg-30">Stewart Butterfield</h3>
            <p>Co-fondateur et CEO<br>FitBit</p>
          </a>
          <a href="#" class="d-block persona">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/orateur.png" alt="" class="w-100">
            <h3 class="mt-15 mt-lg-30 color-rose">Stewart Butterfield</h3>
            <p>Co-fondateur et CEO<br>FitBit</p>
          </a>
          <a href="#" class="d-block persona">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/orateur.png" alt="" class="w-100">
            <h3 class="mt-15 mt-lg-30">Stewart Butterfield</h3>
            <p>Co-fondateur et CEO<br>FitBit</p>
          </a>
        </div>
      </div><!-- /.col -->
      
      <div class="col-12 mt-20 mt-lg-30" data-aos="fade-up">
        <a href="#" class="btn-underline color-violet">Voir tous les orateurs</a>
      </div><!-- /.col -->

    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.container-fluid -->


<div class="container-fluid background-white py-50 py-lg-100">
  <div class="container">
    <div class="row">
      
      <div class="col-12">
        <h1 class="color-violet" data-aos="fade-down">Partenaires</h1>
      </div><!-- /.col -->
      
      <div class="col-12 mt-20 mt-lg-30 text-lg-center" data-aos="fade-down">
        <h3 class="color-gray-dark">Partenaires Silver</h3>
      </div><!-- /.col -->
      
      <div class="col-12 d-flex flex-wrap justify-content-start justify-content-lg-center" data-aos="fade-down">
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-1.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-2.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-3.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-4.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-1.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-2.png" alt=""></div>
      </div><!-- /.col -->
      
      <div class="col-12 mt-20 mt-lg-30 text-lg-center" data-aos="fade-down">
        <h3 class="color-gray-dark">Partenaires Gold</h3>
      </div><!-- /.col -->
      
      <div class="col-12 d-flex flex-wrap justify-content-start justify-content-lg-center" data-aos="fade-down">
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-1.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-2.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-3.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-4.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-1.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-2.png" alt=""></div>
        <div class="p-10 p-sm-20 partenaire-img"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/partenaire-3.png" alt=""></div>
      </div><!-- /.col -->
      
      <div class="col-12 mt-20 mt-lg-30 text-lg-center" data-aos="fade-up">
        <a href="#" class="btn-underline color-violet">Voir tous les partenaires</a>
      </div><!-- /.col -->

    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.container-fluid -->
