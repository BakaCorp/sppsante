<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles;
?>

<?php while (have_posts()) : the_post(); ?>

  <div class="wrap pb-100 mb-lg-100" role="document">
    <div class="row">

      <div class="sidebar-menu d-none d-xl-flex col-xl-3 justify-content-center align-items-start">
        <div><?php include Wrapper\sidebar_path(); ?></div>
      </div><!-- /.sidebar -->

      <div class="page-content col-lg-12 col-xl-9">
        <?php get_template_part('templates/single', 'header'); ?>
        <?php get_template_part('templates/content', 'page'); ?>

        <div class="col-12" data-aos='fade-up'>
          <div class="separate custom-pagination d-flex justify-content-between align-items-center">
            <?php
            $next_post = get_next_post();
            $previous_post = get_previous_post();
            ?>
            <?php if (!empty( $previous_post )): ?>
              <a href="<?php echo $previous_post->guid ?>"><svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-right"></use></svg></a>
            <?php endif; ?>
            <?php if (!empty( $next_post )): ?>
              <a href="<?php echo $next_post->guid ?>"><svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-left"></use></svg></a>
            <?php endif; ?>
          </div>
        </div>
      </div><!-- /.main -->
      
    </div><!-- /.content -->
  </div><!-- /.wrap -->

<?php endwhile; ?>