<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles;
?>

<div class="wrap pb-100 mb-lg-100" role="document">
  <div class="row">

      <div class="page-content col-12">  
        <div class="page-header">
          <div class="page-header-nopicture"></div>
          <div class="page-header-title col-12 col-xl-10 mx-xl-auto">
            <h1><?php _e('Erreur 404', 'sppsante'); ?></h1>
            <?php _e('Désolé, la page demandée n\'existe pas...', 'sppsante'); ?>
            <p class="text-center mt-50"><a href="<?= esc_url(home_url()); ?>" class="btn"><?= _e( 'Retour à l\'accueil', 'sppsante' ); ?></a></p>
          </div>
        </div>
      </div><!-- /.main -->
    
  </div><!-- /.row -->
</div><!-- /.wrap -->