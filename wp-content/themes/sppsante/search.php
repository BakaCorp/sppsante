<div class="container-fluid background-gray">
  <div class="container">
  
    <div class="row">
      <div class="col-12 p-30 text-center d-flex justify-content-center align-items-center">
        <?php get_search_form(); ?>
      </div>
    </div>

  </div>
</div>

<div class="col-12 col-xl-10 mx-xl-auto py-50">
  <div class="row">
    <div class="col-12 col-sm-8">
      
      <?php
      $argsSearchPage = array(
        'numberposts'	    => 10,
        'post_type'		    => 'page',
        's'    						=> get_search_query(),
        'post_status'     => 'publish',
        'order'           => 'ASC',
        'orderby'         => 'menu_order',
      );
      $getSearchPage = get_posts($argsSearchPage);
      ?>
  
      <?php if ( $getSearchPage ) : ?>
        <h2 class="color-rose mb-30"><?php _e('Pages', 'sppsante'); ?></h2>
        <?php foreach ($getSearchPage as $indexSearchPage => $theSearchPage) : ?>
          <?php $post = get_post( $theSearchPage->ID ); setup_postdata( $post ); ?>
          <a href="<?php the_permalink(); ?>" <?php post_class('d-block border-bottom pb-30 mb-30'); ?>>
            <div class="entry-content small">
              <h3 class="color-violet"><?php the_title(); ?></h3>
              <?php echo get_the_excerpt(); ?>
            </div>
          </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
      
      <?php
      $argsSearchPost = array(
        'numberposts'	    => 10,
        'post_type'		    => 'post',
        's'    						=> get_search_query(),
        'post_status'     => 'publish',
        'order'           => 'ASC',
        'orderby'         => 'menu_order',
      );
      $getSearchPost = get_posts($argsSearchPost);
      ?>
  
      <?php if ( $getSearchPost ) : ?>
        <h2 class="color-rose mt-50 mb-30"><?php _e('Articles', 'sppsante'); ?></h2>
        <?php foreach ($getSearchPost as $indexSearchPost => $theSearchPost) : ?>
          <?php $post = get_post( $theSearchPost->ID ); setup_postdata( $post ); ?>
          <a href="<?php the_permalink(); ?>" <?php post_class('d-block border-bottom pb-30 mb-30'); ?>>
            <div class="entry-content small">
              <h3 class="color-violet"><?php the_title(); ?></h3>
              <?php echo get_the_excerpt(); ?>
            </div>
          </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    </div>

    <div class="col-12 col-sm-4">
      
      <?php
      $argsSearchProgramme = array(
        'numberposts'	    => 10,
        'post_type'		    => 'programmes',
        's'    						=> get_search_query(),
        'post_status'     => 'publish',
        'order'           => 'ASC',
        'orderby'         => 'menu_order',
      );
      $getSearchProgramme = get_posts($argsSearchProgramme);
      ?>
  
      <?php if ( $getSearchProgramme ) : ?>
        <h2 class="color-rose mb-30"><?php _e('Programme', 'sppsante'); ?></h2>
        <?php foreach ($getSearchProgramme as $indexSearchProgramme => $theSearchProgramme) : ?>
          <?php $post = get_post( $theSearchProgramme->ID ); setup_postdata( $post ); ?>
          <a href="<?php the_permalink('53'); ?>" <?php post_class('d-block border-bottom pb-30 mb-30'); ?>>
            <div class="entry-content small">
              <h3 class="color-violet"><?php the_title(); ?></h3>
              <?php echo get_the_excerpt(); ?>
            </div>
          </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
      
      <?php
      $argsSearchOrateurs = array(
        'numberposts'	    => 10,
        'post_type'		    => 'intervenants',
        's'    						=> get_search_query(),
        'post_status'     => 'publish',
        'order'           => 'ASC',
        'orderby'         => 'menu_order',
      );
      $getSearchOrateurs = get_posts($argsSearchOrateurs);
      ?>
  
      <?php if ( $getSearchOrateurs ) : ?>
        <h2 class="color-rose mt-50 mb-30"><?php _e('Orateurs', 'sppsante'); ?></h2>
        <?php foreach ($getSearchOrateurs as $indexSearchOrateurs => $theSearchOrateurs) : ?>
          <?php $post = get_post( $theSearchOrateurs->ID ); setup_postdata( $post ); ?>
          <a href="<?php the_permalink('50'); ?>" <?php post_class('d-block border-bottom pb-30 mb-30'); ?>>
            <div class="entry-content small">
              <h3 class="color-violet"><?php the_title(); ?></h3>
              <?php echo get_the_excerpt(); ?>
            </div>
          </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    </div>
  </div>
</div>