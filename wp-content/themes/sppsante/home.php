<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles;
?>

<div class="wrap pb-100 mb-lg-100" role="document">
  <div class="row">

    <div class="sidebar-menu d-none d-xl-flex col-xl-3 justify-content-center align-items-start">
      <div><?php include Wrapper\sidebar_path(); ?></div>
    </div><!-- /.sidebar -->

    <div class="page-content col-12 col-xl-9">
      <div class="page-header">
        <?php 
        if(get_the_post_thumbnail()) {
          $page_header_xs = get_the_post_thumbnail_url( get_the_ID(), 'page-header-xs' );
          $page_header_sm = get_the_post_thumbnail_url( get_the_ID(), 'page-header-sm' );
          $page_header_xl = get_the_post_thumbnail_url( get_the_ID(), 'page-header-xl' );
          ?>
          <picture>
            <source media="(max-width: 767px)" srcset="<?php echo esc_attr( $page_header_xs ); ?>">
            <source media="(max-width: 991px)" srcset="<?php echo esc_attr( $page_header_sm ); ?>">
            <?php the_post_thumbnail( 'page-header-xl', ['class' => 'w-100']  ); ?>
          </picture>
          <?php
        } else {
          echo '<div class="page-header-nopicture"></div>';
        }
        ?>
        
        <div class="page-header-title col-12 col-xl-10 mx-xl-auto">
          <h1><?= Titles\title(); ?></h1>
        </div>
        
      </div>

      <div class="col-12">
        <div class="row">
          <?php $count = 0; while (have_posts()) : the_post(); $count++; ?>
            <?php if($count %2) : ?>
              <div class="col-12 col-sm-7">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php else: ?>
              <div class="col-12 col-sm-5">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php endif; ?>
          <?php endwhile; ?>
          <?php $count = 0; while (have_posts()) : the_post(); $count++; ?>
            <?php if($count %2) : ?>
              <div class="col-12 col-sm-7">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php else: ?>
              <div class="col-12 col-sm-5">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php endif; ?>
          <?php endwhile; ?>
          <?php $count = 0; while (have_posts()) : the_post(); $count++; ?>
            <?php if($count %2) : ?>
              <div class="col-12 col-sm-7">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php else: ?>
              <div class="col-12 col-sm-5">
                <?php include TEMPLATEPATH . '/templates/content-single.php'; ?>
              </div>
            <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div><!-- /.main -->
    
  </div><!-- /.content -->
</div><!-- /.wrap -->
