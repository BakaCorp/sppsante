<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

if (get_field('inactive') == true) {
  wp_redirect( esc_url(home_url('/')) );
  exit;
} elseif(get_the_ID() == 64) {
  wp_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
  exit;
}
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <div class="d-none" id="data-templatedirectory" data-templatedirectory="<?php echo get_template_directory_uri(); ?>"></div>
    <?php get_template_part('templates/content-svg'); ?>

    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

    <?php include Wrapper\template_path(); ?>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
