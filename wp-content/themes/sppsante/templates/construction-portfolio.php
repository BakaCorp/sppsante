<div class="mt-50 mt-sm-60 mt-lg-100 pt-50 pt-sm-60 pt-lg-100 pb-20 pb-sm-20 pb-lg-40 background-gray-light">
  
  <div class="col-12 col-xl-10 mx-xl-auto">
    <div class="row titre-decale">
      <?php if( $element['titre'] ): ?>
        <h2 class="col-12 mb-20 mb-sm-30" data-aos="fade-down"><?php echo $element['titre']; ?></h2>
      <?php endif; ?>
      <?php if( $blocs = $element['blocs'] ): ?>
        <?php foreach ($blocs as $numero => $persona) : ?>
          <?php if( $persona['lien'] ): ?>
            <a href="<?php echo $persona['lien']; ?>" class="persona col-6 col-sm-4 col-lg-3 mb-30 mb-sm-40 mb-lg-60" data-aos="fade-left">
              <div>
                <?php echo wp_get_attachment_image( $persona['image'], 'square', false, ['class' => 'w-100'] ); ?>
                <?php if( $persona['titre'] ): ?>
                  <h3 class="mt-15 mt-lg-30"><?php echo $persona['titre']; ?></h3>
                <?php endif; ?>
                <?php if( $persona['texte'] ): ?>
                  <p><?php echo $persona['texte']; ?></p>
                <?php endif; ?>
              </div>
            </a>
          <?php else: ?>
            <div class="persona col-6 col-sm-4 col-lg-3 mb-30 mb-sm-40 mb-lg-60" data-aos="fade-left">
              <div>
                <?php echo wp_get_attachment_image( $persona['image'], 'square', false, ['class' => 'w-100'] ); ?>
                <?php if( $persona['titre'] ): ?>
                  <h3 class="mt-15 mt-lg-30"><?php echo $persona['titre']; ?></h3>
                <?php endif; ?>
                <?php if( $persona['texte'] ): ?>
                  <p><?php echo $persona['texte']; ?></p>
                <?php endif; ?>
              </div>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</div>