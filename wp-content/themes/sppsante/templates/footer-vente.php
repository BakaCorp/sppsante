<!-- Site Footer -->
<footer class="content-info container-fluid my-50 py-30 py-lg-50 text-center small">
  <?php _e('© SPP Santé 2018, Tous droits réservés','sppsante'); ?>
  <?php echo '<a href="'.get_permalink( 705 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 705 ).'</a>'; ?>
  <?php echo '<a href="'.get_permalink( 30 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 30 ).'</a>'; ?>
  <?php echo '<a href="'.get_permalink( 3 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 3 ).'</a>'; ?>
</footer><!-- /footer -->


<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/custom/logotypespp.js"></script>