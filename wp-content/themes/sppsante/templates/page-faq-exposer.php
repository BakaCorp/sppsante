<!-- CONTENT PAGE QUESTIONS -->

<?php
//args global
$argsQuestions = array(
  'posts_per_page' => -1,
  'post_type'		=> 'faq-exposer',
  'post_status'     => 'publish',
  'order'           => 'ASC',
  'orderby'         => 'meta_value',
  'meta_key'        => 'categorie',
);

//Get posts
$getQuestions = get_posts($argsQuestions);

$theCategory = false;
?>

<?php if ( $getQuestions ) : ?>

  <div class="col-12 col-xl-10 mx-xl-auto">
      <?php foreach ($getQuestions as $indexQuestion => $theQuestion) : ?>
        <?php $post = get_post( $theQuestion->ID ); setup_postdata( $post ); ?>
        <?php if( $theCategory != get_field('categorie') ): $theCategory = get_field('categorie'); ?>
          <h2 class="color-rose mt-50" data-aos="fade-right"><?php the_field( 'categorie' ); ?></h2>
        <?php endif; ?>

        <div class="row">
          <div class="question col-12" data-aos="fade-left">
            <a data-toggle="collapse" href="#collapseQuestion<?php echo $theQuestion->ID; ?>">
              <?php the_title(); ?>
            </a>
            <div class="collapse small" id="collapseQuestion<?php echo $theQuestion->ID; ?>">
              <?php the_content(); ?>
              <?php if( get_field('lien') ): ?>
                <a href="<?php the_field('lien'); ?>" class="link-underline mt-20"><?php _e('En savoir plus','sppsante'); ?></a>
              <?php endif; ?>
            </div>
          </div>
        </div>        
      <?php endforeach; ?>

  </div>
  <?php wp_reset_postdata(); ?>

  <?php foreach ($getQuestions as $theQuestion) : ?>
    <?php $post = get_post( $theQuestion->ID ); setup_postdata( $post ); ?>
    <?php include TEMPLATEPATH . '/templates/sidebar-intervenant.php'; ?>

    <?php if ( $getProgrammes ) : ?>
      <?php foreach ($getProgrammes as $indexProgramme => $theProgramme) : ?>
        <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
        <?php include TEMPLATEPATH . '/templates/sidebar-programme.php'; ?>      
      <?php endforeach; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>

  <div class="back-sidebar"></div>
<?php endif; ?>