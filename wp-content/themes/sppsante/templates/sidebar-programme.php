<div class="sidebar-persona" id="sidebar-<?php the_ID(); ?>">
  <a href="#" class="sidebar-persona-close"><svg class="svg-15 svg-sm-25 fill-white"><use xlink:href="#svg-cross"></use></svg></a>
  <?php the_title( '<h2 class="mt-15 mt-lg-30 color-rose">', '</h2>' ); ?>
  <div class="d-flex justify-content-between align-items-center color-white">
    <h4><?php echo get_field('heure_de_debut').' - '.get_field('heure_de_fin'); ?></h4>
    <?php the_field('lieu'); ?>
  </div>
  <div class="sidebar-persona-content small color-white mt-30"><?php the_content(); ?></div>

  <?php if($intervenant = get_field('intervenant')) : ?>
    <h4 class="color-violet mt-40 mb-15"><?php _e('Cette conférence est animée par :','sppsante'); ?></h4>
    <a href="#sidebar-<?php echo $intervenant->ID; ?>" class="active-sidebar d-flex align-items-end color-white">
      <?php $thumb = get_the_post_thumbnail( $intervenant->ID, 'thumbnail', array( 'class' => 'svg-75' ) ); print_r($thumb); ?>
      <div class="pl-15">
        <h4><?php echo $intervenant->post_title; ?></h4>
        <div class="link-underline color-white m-0 small"><?php _e('En savoir plus','sppsante'); ?></div>
      </div>
    </a>
  <?php endif; ?>
</div>
