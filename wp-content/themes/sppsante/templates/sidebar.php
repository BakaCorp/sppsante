<?php 
$idParent = wp_get_post_parent_id(get_the_ID());
if( is_singular('communiques') || get_post_field( 'post_name' ) == 'communiques' ) {
  $idParent = 57;
} else if( is_single() || is_home() ) {
  $idParent = 8;
}
$pages_brother = get_pages( 
  array( 'child_of' => $idParent, 
  'sort_order' => 'asc',
  'sort_column' => 'menu_order'
  )
);
?>
<?php foreach ($pages_brother as $page_brother) : ?>
  <div>
    <a class="d-inline-block mb-20<?php if(get_the_ID() == $page_brother->ID || ( (is_single() || is_home() ) && $page_brother->ID == get_option( 'page_for_posts' ))) echo ' active'; ?><?php if(get_field('inactive', $page_brother->ID) == true ) echo ' inactive'; ?>" href="<?php echo get_permalink( $page_brother ); ?>"><?php echo get_the_title( $page_brother ); ?></a>
  </div>
<?php endforeach; ?>

<?php if ($idParent == '55') : ?>
  <hr>
  <a href="<?= esc_url(home_url('/product-category/obtenir-un-pass/')); ?>" target="_blank" class="btn mt-20"><?php _e('Obtenir un pass','sppsante'); ?></a>
<?php elseif ($idParent == '18') : ?>
  <hr>
  <a href="<?= get_permalink( 226 ) ?>" target="_blank" class="btn mt-20"><?php _e('Réserver votre stand','sppsante'); ?></a>
<?php elseif ($idParent == '57') : ?>
  <hr>
  <a href="<?= get_permalink( 74 ) ?>" target="_blank" class="btn mt-20"><?php _e('Votre accréditation','sppsante'); ?></a>
<?php endif; ?>