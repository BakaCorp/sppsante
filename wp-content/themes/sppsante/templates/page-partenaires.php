<!-- CONTENT PAGE PARTENAIRES -->

<div class="col-12 col-xl-10 mx-xl-auto">

  <?php
  $getPartenairesGlobaux = get_posts(array(
    'numberposts'	=> 5,
    'post_type'		=> 'partenaires',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'title',
    'meta_query' => array(
      array(
        'key'     => 'partenaire_type',
        'value'   => 'Globaux',
        'compare' => '==',
      )
    )
  ));
  ?>

  <?php if( $getPartenairesGlobaux ): ?>

    <div class="row">
      <div class="col-12 d-flex flex-wrap justify-content-start align-items-center" data-aos="fade-up">
        <?php foreach ($getPartenairesGlobaux as $thePartenaireGlobaux) : ?>
          <?php $post = get_post( $thePartenaireGlobaux->ID ); setup_postdata( $post ); ?>
          <?php if(get_field('lien')) : ?>
            <a href="<?php the_field('lien'); ?>" target="_blank" class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></a>
          <?php else: ?>
            <div class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div><!-- /.col -->
    </div><!-- /.row -->

  <?php endif; ?>

  <?php
  $getPartenairesGold = get_posts(array(
    'numberposts'	=> -1,
    'post_type'		=> 'partenaires',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'title',
    'meta_query' => array(
      array(
        'key'     => 'partenaire_type',
        'value'   => 'Gold',
        'compare' => '==',
      )
    )
  ));
  ?>

  <?php if( $getPartenairesGold ): ?>

    <div class="row">
      <div class="col-12 mt-20 mt-lg-30" data-aos="fade-down">
        <h2 class="color-rose"><?php _e('Gold','sppsante'); ?></h2>
      </div><!-- /.col -->
      <div class="col-12 d-flex flex-wrap justify-content-start align-items-center" data-aos="fade-up">
        <?php foreach ($getPartenairesGold as $thePartenaireGold) : ?>
          <?php $post = get_post( $thePartenaireGold->ID ); setup_postdata( $post ); ?>
          <?php if(get_field('lien')) : ?>
            <a href="<?php the_field('lien'); ?>" target="_blank" class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></a>
          <?php else: ?>
            <div class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div><!-- /.col -->
    </div><!-- /.row -->

  <?php endif; ?>

  <?php
  $getPartenairesSilver = get_posts(array(
    'numberposts'	=> -1,
    'post_type'		=> 'partenaires',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'title',
    'meta_query' => array(
      array(
        'key'     => 'partenaire_type',
        'value'   => 'Silver',
        'compare' => '==',
      )
    )
  ));
  ?>

  <?php if( $getPartenairesSilver ): ?>

    <div class="row">
      <div class="col-12 mt-20 mt-lg-30" data-aos="fade-down">
        <h2 class="color-rose"><?php _e('Silver','sppsante'); ?></h2>
      </div><!-- /.col -->
      <div class="col-12 d-flex flex-wrap justify-content-start align-items-center" data-aos="fade-up">
        <?php foreach ($getPartenairesSilver as $thePartenaireSilver) : ?>
          <?php $post = get_post( $thePartenaireSilver->ID ); setup_postdata( $post ); ?>
          <?php if(get_field('lien')) : ?>
            <a href="<?php the_field('lien'); ?>" target="_blank" class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></a>
          <?php else: ?>
            <div class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div><!-- /.col -->
    </div><!-- /.row -->

  <?php endif; ?>

  <?php
  $getPartenairesBronze = get_posts(array(
    'numberposts'	=> -1,
    'post_type'		=> 'partenaires',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'title',
    'meta_query' => array(
      array(
        'key'     => 'partenaire_type',
        'value'   => 'Bronze',
        'compare' => '==',
      )
    )
  ));
  ?>

  <?php if( $getPartenairesBronze ): ?>

    <div class="row">
      <div class="col-12 mt-20 mt-lg-30" data-aos="fade-down">
        <h2 class="color-rose"><?php _e('Bronze','sppsante'); ?></h2>
      </div><!-- /.col -->
      <div class="col-12 d-flex flex-wrap justify-content-start align-items-center" data-aos="fade-up">
        <?php foreach ($getPartenairesBronze as $thePartenaireBronze) : ?>
          <?php $post = get_post( $thePartenaireBronze->ID ); setup_postdata( $post ); ?>
          <?php if(get_field('lien')) : ?>
            <a href="<?php the_field('lien'); ?>" target="_blank" class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></a>
          <?php else: ?>
            <div class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div><!-- /.col -->
    </div><!-- /.row -->

  <?php endif; ?>

  <?php
  $getPartenairesMedias = get_posts(array(
    'numberposts'	=> 5,
    'post_type'		=> 'partenaires',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'title',
    'meta_query' => array(
      array(
        'key'     => 'partenaire_type',
        'value'   => 'Medias',
        'compare' => '==',
      )
    )
  ));
  ?>

  <?php if( $getPartenairesMedias ): ?>

    <div class="row">
      <div class="col-12 mt-20 mt-lg-30" data-aos="fade-down">
        <h2 class="color-rose"><?php _e('Médias','sppsante'); ?></h2>
      </div><!-- /.col -->
      <div class="col-12 d-flex flex-wrap justify-content-start align-items-center" data-aos="fade-up">
        <?php foreach ($getPartenairesMedias as $thePartenaireMedias) : ?>
          <?php $post = get_post( $thePartenaireMedias->ID ); setup_postdata( $post ); ?>
          <?php if(get_field('lien')) : ?>
            <a href="<?php the_field('lien'); ?>" target="_blank" class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></a>
          <?php else: ?>
            <div class="p-10 p-sm-20 partenaire-img"><?php the_post_thumbnail( 'logo' ); ?></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div><!-- /.col -->
    </div><!-- /.row -->

  <?php endif; ?>

</div>