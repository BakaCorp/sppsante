<?php 
/*
* Template custom page if exist
*/
get_template_part('templates/page', get_the_ID());
get_template_part('templates/page', get_post_field( 'post_name' ));
?>

<?php
/*
* Construit la page par succession de blocs
*/
$construction = get_field('construction');
if( $construction && ( (is_user_logged_in() && get_the_ID() == 452) || (get_the_ID() != 452) ) ) {
  foreach ($construction as $element) {
    include TEMPLATEPATH . '/templates/construction-' . $element['acf_fc_layout'] . '.php';
  }
}
?>