<!-- CONTENT PAGE PROGRAMME -->

<div class="col-12 col-xl-10 mx-xl-auto">

  <div class="row">
    <div class="scrollx-mobile col-12 pt-50">
      <a class="link-days link-underline big mr-10 mr-sm-20 mr-lg-40 active" href="#dayjeudi"><?php _e('Jeudi 31 Janvier','sppsante'); ?></a>
      <a class="link-days link-underline big mr-10 mr-sm-20 mr-lg-40" href="#dayvendredi"><?php _e('Vendredi 01 Février','sppsante'); ?></a>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-sm-6 py-10 text-sm-right order-sm-2">
      <?php if(get_field('programme_en_pdf','options')) :?>
        <a href="<?php the_field('programme_en_pdf','options'); ?>" target="_blank" class="btn"><?php _e('Télécharger le programme','sppsante'); ?></a>
      <?php endif; ?>
    </div>
    <div class="col-12 col-sm-6 py-30 order-sm-1">
      <div class="dropdown-group">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php
          if($_GET['cat']) {
            echo get_cat_name( $_GET['cat'] );
          } else {
            _e('Toutes les catégories','sppsante');
          }
          ?>
        </a>
        <div class="dropdown-menu">
          <?php
          $categories = get_categories(array(
            'orderby' => 'name',
            'order' => 'ASC', 
            'hide_empty' => true, 
            'exclude' => '1' 
          ));
          ?>
          <a class="dropdown-item" href="<?php the_permalink(); ?>"><?php _e('Toutes les catégories','sppsante'); ?></a>
          <?php foreach( $categories as $category ) : ?>
            <a class="dropdown-item" href="?cat=<?php echo $category->term_id; ?>"><?php echo $category->name; ?></a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>

  <?php
  //args global jeudi
  $argsProgrammeJeudi = array(
    'posts_per_page' => -1,
    'post_type'		    => 'programmes',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'meta_value',
    'meta_key'        => 'heure_de_debut',
    'meta_query' => array(
      array(
        'key'     => 'date',
        'value'   => 'jeudi',
        'compare' => '=',
      )
    )
  );

  //if category
  if($_GET['cat']) $argsProgrammeJeudi['category'] = $_GET['cat'];

  //Get posts
  $getProgrammeJeudi = get_posts($argsProgrammeJeudi);
  ?>

  <?php if ( $getProgrammeJeudi ) : ?>
    <?php foreach ($getProgrammeJeudi as $indexProgramme => $theProgramme) : ?>
      <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>

      <a href="#sidebar-<?php the_ID(); ?>" class="row day-programme dayjeudi active active-sidebar py-30 border-bottom" data-aos="fade-left">
        <div class="col-12 col-sm-4 col-lg-3 border-right">
          <h3 class="color-rose"><?php echo get_field('heure_de_debut').' - '.get_field('heure_de_fin'); ?></h3>
          <?php the_field('lieu'); ?>
        </div>
        <div class="col-12 col-sm-8 col-lg-9">
          <?php the_title( '<h3 class="color-rose">', '</h3>' ); ?>
          <div class="small">
            <p><?php echo get_the_excerpt(); ?></p>
            <?php if($intervenant = get_field('intervenant')) { echo '<strong>'.__('Animé par','sppsante').' '.$intervenant->post_title.'</strong>'; } ?>
          </div>
        </div>
      </a>
      
    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>


  <?php
  //args global vendredi
  $argsProgrammeVendredi = array(
    'posts_per_page' => -1,
    'post_type'		    => 'programmes',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'meta_value',
    'meta_key'        => 'heure_de_debut',
    'meta_query' => array(
      array(
        'key'     => 'date',
        'value'   => 'vendredi',
        'compare' => '=',
      )
    )
  );

  //if category
  if($_GET['cat']) $argsProgrammeVendredi['category'] = $_GET['cat'];

  //Get posts
  $getProgrammeVendredi = get_posts($argsProgrammeVendredi);
  ?>

  <?php if ( $getProgrammeVendredi ) : ?>
    <?php foreach ($getProgrammeVendredi as $indexProgramme => $theProgramme) : ?>
      <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>

      <a href="#sidebar-<?php the_ID(); ?>" class="row day-programme dayvendredi active-sidebar py-30 border-bottom" data-aos="fade-left">
        <div class="col-12 col-sm-4 col-lg-3 border-right">
          <h3 class="color-rose"><?php echo get_field('heure_de_debut').' - '.get_field('heure_de_fin'); ?></h3>
          <?php the_field('lieu'); ?>
        </div>
        <div class="col-12 col-sm-8 col-lg-9">
          <?php the_title( '<h3 class="color-rose">', '</h3>' ); ?>
          <div class="small">
            <?php the_content(); ?>
            <?php if($intervenant = get_field('intervenant')) { echo '<strong>'.__('Animé par','sppsante').' '.$intervenant->post_title.'</strong>'; } ?>
          </div>
        </div>
      </a>
      
    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>

</div>


<?php
//args global
$argsProgramme = array(
  'posts_per_page' => -1,
  'post_type'		    => 'programmes',
  'post_status'     => 'publish'
);

//if category
if($_GET['cat']) $argsProgramme['category'] = $_GET['cat'];

//Get posts
$getProgramme = get_posts($argsProgramme);
?>

<?php if ( $getProgramme ) : ?>
  <?php foreach ($getProgramme as $indexProgramme => $theProgramme) : ?>
    <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
    <?php include TEMPLATEPATH . '/templates/sidebar-programme.php'; ?>
    
    <?php if($intervenant = get_field('intervenant')) : ?>
      <?php $post = get_post( $intervenant->ID ); setup_postdata( $post ); ?>
      <?php include TEMPLATEPATH . '/templates/sidebar-intervenant.php'; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
    
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>

  <div class="back-sidebar"></div>
<?php endif; ?>