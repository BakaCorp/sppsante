<header class="vente">
  <div class="text-center">
    <a class="brand text-left" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-full-rvb"></use></svg></a>
    <a class="brand-dates" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-dates-rvb"></use></svg></a>
  </div>
</header>
