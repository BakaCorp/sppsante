<!-- Alert Site Under Construction -->
<div id="alert-inactive" class="alert alert-secondary alert-dismissible" role="alert">
  <?php the_field('alerte_site_en_construction','options'); ?>
  <button type="button" class="alert-close close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<!-- Site Footer Contact -->
<div id="contact" class="footer-contact container-fluid background-violet py-30 py-lg-50">
  <div class="container my-30 my-lg-50">
    <div class="row">
      <div class="col-12 col-lg color-white">
        <div id="formContainer_KGElJ85L7kvoW6Q"></div><script src="https://public.message-business.com/Javascript/form/MB_Form_JsApp.js?v=KGElJ85L7kvoW6Q"></script><script>var MB_Form_JsApp = new MB_Form_JsApp();MB_Form_JsApp.ContainerId = "formContainer_KGElJ85L7kvoW6Q";MB_Form_JsApp.AccountId = "53571";MB_Form_JsApp.OperationId = "52"; MB_Form_JsApp.OperationType = "form"; MB_Form_JsApp.Init();</script>
      </div>
      <div class="col-12 col-lg-auto d-lg-flex align-items-lg-end">
        <div class="color-white w-100 text-lg-right py-10 pl-lg-30 border-lg-left">
          <span class="d-block d-lg-inline-block mr-15"><?php _e('Suivez-nous','sppsante'); ?></span>
          <?php if(get_field('facebook','options')) :?>
            <a href="<?php the_field('facebook','options'); ?>" target="_blank" class="color-white"><svg class="svg-35"><use xlink:href="#svg-facebook"></use></svg></a>
          <?php endif; ?>
          <?php if(get_field('twitter','options')) :?>
            <a href="<?php the_field('twitter','options'); ?>" target="_blank" class="color-white"><svg class="svg-35"><use xlink:href="#svg-twitter"></use></svg></a>
          <?php endif; ?>
          <?php if(get_field('linkedin','options')) :?>
            <a href="<?php the_field('linkedin','options'); ?>" target="_blank" class="color-white"><svg class="svg-35"><use xlink:href="#svg-linkedin"></use></svg></a>
          <?php endif; ?>
          <?php if(get_field('youtube','options')) :?>
            <a href="<?php the_field('youtube','options'); ?>" target="_blank" class="color-white"><svg class="svg-35"><use xlink:href="#svg-youtube"></use></svg></a>
          <?php endif; ?>
        </div>
      </div>
    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.container-fluid -->

<!-- Site Footer -->
<footer class="content-info container-fluid py-30 py-lg-50 background-gray-light small">
  <div class="container">

    <div class="row">

      <div class="col-12 col-lg-3 text-center text-lg-left mb-20 mb-lg-0">
        <a class="brand mb-10" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-full-rvb"></use></svg></a>
        <div class="color-violet font-weight-bold"><?php _e('31 janvier —  1 février 2019<br>Palais des Congrès, ','sppsante'); ?></div>
        <div class="color-rose font-weight-bold"><?php _e('Bordeaux','sppsante'); ?></div>
      </div>

      <div class="col-12 col-lg-9">
        <div class="row">
          <div class="col-6 col-sm-4 col-lg-3">
            <?php
            $pages = array(8,50,53);
            include TEMPLATEPATH . '/templates/footer-menu.php';
            ?>
          </div>
          <div class="col-6 col-sm-4 col-lg-3">
            <?php
            $pages = array(55,18);
            include TEMPLATEPATH . '/templates/footer-menu.php';
            ?>
          </div>
          <div class="col-6 col-sm-4 col-lg-3">
            <?php
            $pages = array(57);
            include TEMPLATEPATH . '/templates/footer-menu.php';
            ?>
          </div>
          <div class="col-6 col-sm-4 col-lg-3 d-flex flex-column align-items-start">
            <div>
              <?php
              $pages = array(80,28,78,26);
              include TEMPLATEPATH . '/templates/footer-menu.php';
              ?>
            </div>
            <div class="mt-auto">
              <a href="#top" class="scrollTop"><svg class="svg-20"><use xlink:href="#svg-arrow-top"></use></svg> <?php _e('Haut de page','sppsante'); ?></a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
    
    <div class="row mt-5 mb-5">
      <div class="col-12">
        <hr>
      </div>
    </div><!-- /.row -->
    
    <div class="row mt-5 mb-5">
      <div class="col-12 col-lg-6 color-gray-dark text-center text-lg-left">
        <?php _e('© SPP Santé 2018, Tous droits réservés','sppsante'); ?>
      </div>
      <div class="col-12 col-lg-6 text-center text-lg-right">
        <?php echo '<a href="'.get_permalink( 705 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 705 ).'</a>'; ?>
        <?php echo '<a href="'.get_permalink( 30 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 30 ).'</a>'; ?>
        <?php echo '<a href="'.get_permalink( 3 ).'" class="text-nowrap color-gray-dark ml-15">'.get_the_title( 3 ).'</a>'; ?>
      </div>
    </div><!-- /.row -->

  </div><!-- /.container -->
</footer><!-- /footer -->

<div class="sidebar-persona" id="sidebar-login">
  <a href="#" class="sidebar-persona-close"><svg class="svg-15 svg-sm-25 fill-white"><use xlink:href="#svg-cross"></use></svg></a>
  <h2 class="color-white"><?php _e('Connexion','sppsante'); ?></h2>
  <?php include_once(TEMPLATEPATH.'/woocommerce/auth/form-login.php'); ?>
  
  <h3 class="color-white mt-50"><?php _e('Pas encore inscrits ?','sppsante'); ?></h3>
  <a href="<?= get_permalink( 226 ) ?>" target="_blank" class="btn mt-20"><?php _e('Réserver votre stand','sppsante'); ?></a>
</div>


<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/custom/logotypespp.js"></script>