<?php
$getIntervenants = get_posts(array(
  'numberposts'	=> -1,
  'post_type'		=> 'intervenants',
  'post_status'     => 'publish',
  'order'           => 'ASC',
  'orderby'         => 'menu_order',
  'meta_query' => array(
    array(
      'key'     => 'intervenant_cat',
      'value'   => 'comite',
      'compare' => 'LIKE',
    )
  )
));
?>

<?php if ( $getIntervenants ) : ?>
  <div class="mt-50 mt-sm-60 mt-lg-100 pt-50 pt-sm-60 pt-lg-100 pb-20 pb-sm-20 pb-lg-40 background-gray-light"> 
    <div class="col-12 col-xl-10 mx-xl-auto">
      <div class="row titre-decale">
      <h2 class="col-12 mb-20 mb-sm-30" data-aos="fade-down"><?php _e('Comité d’éthique','sppsante'); ?></h2>
      
        <?php foreach ($getIntervenants as $indexIntervenant => $theIntervenant) : ?>
          <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>

          <a href="#sidebar-<?php the_ID(); ?>" class="persona <?php if(get_field('sexe') == 'Femme') echo 'persona-femme'; ?> active-sidebar col-6 col-sm-4 col-lg-3 mb-30 mb-sm-40 mb-lg-60" data-aos="fade-left">
            <div>
              <?php the_post_thumbnail( 'square', ['class' => 'w-100'] ); ?>
              <?php the_title( '<h3 class="mt-15 mt-lg-30">', '</h3>' ); ?>
              <?php if( get_field('intervenant_parcours') ): ?><p><?php the_field( 'intervenant_parcours' ); ?></p><?php endif; ?>
            </div>
          </a>
          
        <?php endforeach; ?>

      </div>
    </div>
  </div>
  <?php wp_reset_postdata(); ?>

  <?php foreach ($getIntervenants as $theIntervenant) : ?>
    <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>
    <?php include TEMPLATEPATH . '/templates/sidebar-intervenant.php'; ?>

    <?php if ( $getProgrammes ) : ?>
      <?php foreach ($getProgrammes as $indexProgramme => $theProgramme) : ?>
        <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
        <?php include TEMPLATEPATH . '/templates/sidebar-programme.php'; ?>      
      <?php endforeach; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>

  <div class="back-sidebar"></div>
<?php endif; ?>