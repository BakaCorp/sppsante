<!-- CONTENT PAGE COMMUNIQUES -->

<?php
//detect the set number of posts per page
$ppp = get_option('posts_per_page');

$argsCommuniques = array(
  'numberposts'	=> -1,
  'post_type'		=> 'communiques',
  'post_status'     => 'publish',
  'order'           => 'DESC',
  'orderby'         => 'date'
);

// Total Count
$totalPosts = get_posts($argsCommuniques);

// first page x posts
$argsCommuniques['numberposts'] = $ppp;
if (!is_paged()) {
  $argsCommuniques['numberposts'] = $ppp;
// second page with offset
} elseif($paged == 2) {
  $argsCommuniques['offset'] = $ppp;
// all other pages with settings from backend
} else {
  $argsCommuniques['offset'] = $ppp*($paged-2)+$ppp;
}

$getCommuniques = get_posts($argsCommuniques);
?>

<?php if ( $getCommuniques ) : ?>

  <div class="col-12 col-xl-10 mx-xl-auto mt-20 mt-sm-30 mt-lg-40">
    <div class="row" data-aos="fade-up">
      <?php foreach ($getCommuniques as $indexCommunique => $theCommunique) : ?>
        <?php $post = get_post( $theCommunique->ID ); setup_postdata( $post ); ?>
        <div class="col-12"><?php include TEMPLATEPATH . '/templates/single-row.php'; ?></div>
      <?php endforeach; ?>
    </div>  
    <?php wp_reset_postdata(); ?>
  </div>

<?php endif; ?>

<?php sppagination( ceil(count($totalPosts)/$ppp) , "" , $paged ); ?>