<!-- CONTENT PAGE PACKAGES -->

<?php
  $argsPackage = array(
    'post_type'       => 'packages',
    'posts_per_page'  => -1,
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'menu_order'
  );
  $getPackages = new WP_Query( $argsPackage );
  ?>

  <?php if ( $getPackages->have_posts() ) : ?>
    <?php while ( $getPackages->have_posts() ): $getPackages->the_post(); ?>
    
      <div class="package col-12 py-30 py-lg-60 <?php if(get_field('populaire')) { echo 'populaire'; } ?>" data-aos="fade-left">
        <div class="row">
          <div class="col-5 col-sm-4 col-lg-3 offset-lg-1">
            <div class="square">
              <div class="square-calc"></div>
              <div class="square-content d-flex justify-content-center align-items-center" style="background-color: <?php the_field('couleur'); ?>;">
                <div><svg class="svg-80 svg-sm-100" style="fill: <?php the_field('couleur_icone'); ?>;"><use xlink:href="#<?php the_field('icon'); ?>"></use></svg></div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-8 mt-20 mt-sm-0">
            <h2 style="color: <?php the_field('couleur'); ?>;"><?php the_title(); ?></h2>
            <?php the_content(); ?>
            <h3 class="color-violet mt-30"><?php the_field('prix'); ?></h3>
            <a href="<?php the_permalink(226); ?>?package=<?php echo get_post_field( 'post_name' ); ?>" target="_blank" class="btn mt-20"><?php _e('Réservez','sppsante'); ?></a>
          </div>
        </div>
      </div>
      
  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>