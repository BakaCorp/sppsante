<a href="<?php the_permalink(); ?>" <?php post_class('d-block'); ?>>
  <header class="background-gray">
    <?php the_post_thumbnail( 'page-header-sm' ); ?>
  </header>
  <div class="entry-content small">
    <time class="small" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date('d.m - Y'); ?></time>
    <h3 class="color-violet"><?php the_title(); ?></h3>
    <?php echo get_the_excerpt(); ?>
  </div>
</a>