<?php
$getActualites = get_posts(array(
  'numberposts'	=> 8,
  'post_type'		=> 'post',
  'post_status'     => 'publish',
  'order'           => 'DESC',
  'orderby'         => 'date'
));
?>

<?php if ( $getActualites ) : ?>
  <div class="row">
    
    <div class="col-12 titre-decale">
      <h1 class="color-violet" data-aos="fade-down"><?php _e('Actualités','sppsante'); ?></h1>
      <div class="row" data-aos="fade-up">
      
        <?php foreach ($getActualites as $indexActualite => $theActualite) : ?>
          <?php $post = get_post( $theActualite->ID ); setup_postdata( $post ); ?>
          <?php if(($indexActualite+1) == 1 ) : ?>
            <div class="col-12 col-lg-8">
              <div class="row">
          <?php elseif(($indexActualite+1) == 5 ) : ?>
              </div>
            </div>
            <div class="col-12 col-lg-4 d-none d-lg-block">

              <?php if ( get_field('encart_pub_actualites','options') ) : ?>
                <div class="container-fluid pb-20 pb-lg-30 text-center">
                  <?php if ( get_field('lien_encart_pub_actualites','options') ) : ?>
                    <a href="<?php the_field('lien_encart_pub_actualites','options'); ?>" target="_blank"><img src="<?php the_field('encart_pub_actualites','options'); ?>" class="mw-100"></a>
                  <?php else: ?>
                    <img src="<?php the_field('encart_pub_actualites','options'); ?>" class="mw-100">
                  <?php endif; ?>
                </div><!-- /.container-fluid -->
                <hr class="mb-30">
              <?php endif; ?>

          <?php endif; ?>
          <?php if(($indexActualite+1) < 5 ) : ?>
            <?php if(($indexActualite+1) %2) : ?>
              <div class="col-12 col-sm-7">
                <?php include TEMPLATEPATH . '/templates/single-card.php'; ?>
              </div>
            <?php else: ?>
              <div class="col-12 col-sm-5">
                <?php include TEMPLATEPATH . '/templates/single-card.php'; ?>
              </div>
            <?php endif; ?>
          <?php else: ?>
            <?php include TEMPLATEPATH . '/templates/single-row.php'; ?>
          <?php endif; ?>
        <?php endforeach; ?>
        </div>

      </div>
    </div>
  </div>
  <?php wp_reset_postdata(); ?>

  <div class="row" data-aos="fade-up">
    <div class="col-12 mt-20 mt-lg-30">
      <a href="<?php the_permalink('actualites'); ?>" class="btn-underline small color-violet"><?php _e('Voir toutes les actualités','sppsante'); ?></a>
    </div><!-- /.col -->
  </div>

<?php endif; ?>