<!-- CONTENT PAGE INFOS PRATIQUES -->

<div class="col-12 col-xl-10 mx-xl-auto mt-20 mt-sm-30 mt-lg-40">
  <?php $themes = ['Lieu','Transports','Hébergement']; ?>
  <div class="scrollx-mobile">
    <?php foreach ($themes as $order => $theme) : ?>
      <?php $indications = get_field('indications_'.sanitize_title($theme)); ?>
      <a class="link-indications link-underline h3 mr-10 mr-sm-30 mr-lg-60 <?php if($order == 0) { echo 'active'; } if($indications == false) { echo 'inactive'; } ?>" href="#<?php echo 'indication_'.sanitize_title($theme); ?>">
        <?php echo $theme; ?>
      </a>
    <?php endforeach; ?>
  </div>
  <?php foreach ($themes as $order => $theme) : ?>
    <?php $indications = get_field('indications_'.sanitize_title($theme)); ?>
    <?php if($indications) : ?>
      <div id="<?php echo 'indication_'.sanitize_title($theme); ?>" class="indication-content <?php if($order == 0) { echo 'active'; } ?>">
        <?php foreach ($indications as $indication) : ?>
          <div class="indication py-30 py-lg-60">
            <h3 class="color-rose"><?php echo $indication['titre']; ?></h3>
            <?php echo $indication['texte']; ?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
  <?php endforeach; ?>
</div>