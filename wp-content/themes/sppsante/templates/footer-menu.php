<?php
foreach ($pages as $page) {
  $pages_children = get_pages( array( 'child_of' => $page ) );
  echo '<div class="mb-20">';
  if($pages_children) {
    echo '<div class="d-block color-violet font-weight-bold mb-1">'.get_the_title( $page ).'</div>';
  } else {
    echo '<a class="d-block font-weight-bold mb-1';
    if( get_field('inactive', $page) == true ) { echo ' inactive'; }
    echo '" href="'.get_permalink( $page ).'">'.get_the_title( $page ).'</a>';
  }
  foreach ($pages_children as $page_children) {
    echo '<a class="d-block mb-1';
    if( get_field('inactive', $page_children) == true ) { echo ' inactive'; }
    echo '" href="'.get_permalink( $page_children ).'">'.get_the_title( $page_children ).'</a>';
  }
  echo '</div>';
}
?>