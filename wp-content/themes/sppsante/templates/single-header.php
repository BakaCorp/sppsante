<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  
  <div class="page-header-title col-12 col-xl-10 mx-xl-auto">

    <div class="mt-100 mb-50 small">
      <?php if ( is_singular('communiques') ) : ?>
        <a href="<?php the_permalink( '70' ); ?>" class="link-underline"><svg class="svg-15"><use xlink:href="#svg-arrow-right"></use></svg> <?php _e('Tous les communiqués','sppsante'); ?></a>
      <?php else: ?>
        <a href="<?php the_permalink( '407' ); ?>" class="link-underline"><svg class="svg-15"><use xlink:href="#svg-arrow-right"></use></svg> <?php _e('Toutes les actualités','sppsante'); ?></a>
      <?php endif; ?>
    </div>

    <h2><?= Titles\title(); ?></h2>
    <?php the_content(); ?>
    <?php if(get_field('pdf')) : ?>
      <a href="<?php the_field('pdf'); ?>" class="btn mt-15" target="_blank">Télécharger le communiqué</a>
    <?php endif; ?>
  </div>
  
</div>
