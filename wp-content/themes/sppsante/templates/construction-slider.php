<div class="mt-50 mt-sm-60 mt-lg-100" data-aos="fade-left">
  <div class="page-slider">
    <?php $images = $element['images']; ?>
    <?php foreach ($images as $numero => $image) : ?>
      <div class="page-slide<?php if($numero == 0) echo ' active'; ?>">
        <?php 
        $page_slider_xs = wp_get_attachment_image_url( $image['image'], 'page-slider-xs' );
        $page_slider_sm = wp_get_attachment_image_url( $image['image'], 'page-slider-sm' );
        $page_slider_xl = wp_get_attachment_image_url( $image['image'], 'page-slider-xl' );
        ?>
        <picture class="pl-15 pl-xl-0">
          <source media="(max-width: 767px)" srcset="<?php echo esc_attr( $page_slider_xs ); ?>">
          <source media="(max-width: 991px)" srcset="<?php echo esc_attr( $page_slider_sm ); ?>">
          <?php echo wp_get_attachment_image( $image['image'], 'page-slider-xl', false, ['class' => 'w-100'] ); ?>
        </picture>
      
        <div class="col-12 col-xl-10 mx-xl-auto py-10">
          <div class="row">
            <?php if(count($images) > 1) : ?>
              <div class="col-auto">
                <a class="d-flex justify-content-center align-items-center slider-next color-violet">
                  <?php echo '<svg class="slider-circle"><circle r="30" cx="32" cy="32"></circle></svg>'.( $numero+1 ); ?>
                </a>
              </div>
            <?php endif; ?>
            <div class="col d-flex align-items-center">
              <?php if( $image['texte'] ): ?>
                <p class="small"><?php echo $image['texte']; ?></p>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>