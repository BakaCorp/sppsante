<div class="mt-50 mt-sm-60 mt-lg-100" data-aos="fade-left">
  <?php if( $element['texte'] ): ?>
    <div class="col-12 col-xl-10">
      <blockquote><?php echo $element['texte']; ?></blockquote>
    </div>
  <?php endif; ?>
</div>