<!-- CONTENT PAGE EXPOSANTS -->

<?php
//detect the set number of posts per page
$ppp = get_option('posts_per_page');

//args global
$argsExposants = array(
  'numberposts'     => -1,
  'post_type'		    => 'exposants',
  'post_status'     => 'publish',
  'order'           => 'ASC',
  'orderby'         => 'title'
);

// Total Count
$totalPosts = get_posts($argsExposants);

// first page x posts
$argsExposants['numberposts'] = $ppp;
if (!is_paged()) {
  $argsExposants['numberposts'] = $ppp;
// second page with offset
} elseif($paged == 2) {
  $argsExposants['offset'] = $ppp;
// all other pages with settings from backend
} else {
  $argsExposants['offset'] = $ppp*($paged-2)+$ppp;
}

//if category
if($_GET['cat']) $argsExposants['category'] = $_GET['cat'];

//Get posts
$getExposants = get_posts($argsExposants);
?>

<?php if ( $getExposants ) : ?>

  <div class="col-12 col-xl-10 mx-xl-auto">
    <div class="row">
      <div class="col-12 py-30">
        <div class="dropdown-group">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php
            if($_GET['cat']) {
              echo get_cat_name( $_GET['cat'] );
            } else {
              _e('Toutes les catégories','sppsante');
            }
            ?>
          </a>
          <div class="dropdown-menu">
            <?php
            $categories = get_categories(array(
              'orderby' => 'name',
              'order' => 'ASC', 
              'hide_empty' => true, 
              'exclude' => '1' 
            ));
            ?>
            <a class="dropdown-item" href="<?php the_permalink(); ?>"><?php _e('Toutes les catégories','sppsante'); ?></a>
            <?php foreach( $categories as $category ) : ?>
              <a class="dropdown-item" href="?cat=<?php echo $category->term_id; ?>"><?php echo $category->name; ?></a>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <?php foreach ($getExposants as $indexExposant => $theExposant) : ?>
        <?php $post = get_post( $theExposant->ID ); setup_postdata( $post ); ?>

        <div class="exposant <?php if(($indexExposant+1)%3 != 0) echo 'border-right'; ?> col-12 col-sm-6 col-lg-4 mb-30 mb-sm-40 mb-lg-60" data-aos="fade-left">
          <?php if( get_field('stand') ): ?><div class="background-gray-dark color-white text-center p-1 mb-10"><?php the_field( 'stand' ); ?></div><?php endif; ?>
          <?php the_post_thumbnail( 'square', ['class' => 'w-100'] ); ?>
          <div class="text-center small">
            <?php the_title( '<h4 class="color-violet mt-10">', '</h4>' ); ?>
            <?php the_content(); ?>
          </div>
        </div>
        
      <?php endforeach; ?>
    </div>

    <?php sppagination( ceil(count($totalPosts)/$ppp) , "" , $paged ); ?>
  </div>
  <?php wp_reset_postdata(); ?>

<?php endif; ?>