<!-- CONTENT PAGE ACTUALITES -->

<?php
//detect the set number of posts per page
$ppp = get_option('posts_per_page');

$argsActualites = array(
  'numberposts'	=> -1,
  'post_type'		=> 'post',
  'post_status'     => 'publish',
  'order'           => 'DESC',
  'orderby'         => 'date'
);

// Total Count
$totalPosts = get_posts($argsActualites);

// first page x posts
$argsActualites['numberposts'] = $ppp;
if (!is_paged()) {
  $argsActualites['numberposts'] = $ppp;
// second page with offset
} elseif($paged == 2) {
  $argsActualites['offset'] = $ppp;
// all other pages with settings from backend
} else {
  $argsActualites['offset'] = $ppp*($paged-2)+$ppp;
}

$getActualites = get_posts($argsActualites);

$arrayClass = array('col-12', 'col-12 col-sm-7', 'col-12 col-sm-5', 'col-12 col-sm-5', 'col-12 col-sm-7');
$countClass = count($arrayClass);
$thisClass = 0;
?>

<?php if ( $getActualites ) : ?>

  <div class="col-12 col-xl-10 mx-xl-auto">
    <div class="row" data-aos="fade-up">
      <?php foreach ($getActualites as $indexActualite => $theActualite) : ?>
        <?php $post = get_post( $theActualite->ID ); setup_postdata( $post ); ?>
        <div class="<?php echo $arrayClass[$thisClass]; ?>">
          <?php
          if($thisClass == 0) {
            include TEMPLATEPATH . '/templates/single-card-xl.php';
          } else {
            include TEMPLATEPATH . '/templates/single-card.php';
          }
          ?>
        </div>
      <?php if($thisClass >= $countClass-1) $thisClass = 0; else $thisClass++; ?>
      <?php endforeach; ?>
    </div>  
    <?php wp_reset_postdata(); ?>

    <?php sppagination( ceil(count($totalPosts)/$ppp) , "" , $paged ); ?>
  </div>

<?php endif; ?>