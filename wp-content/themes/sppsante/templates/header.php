<div id="activereponsive" class="d-lg-none background-white py-15">
  <div class="container-fluid">
    <div class="row justify-content-between align-items-center">
      <div class="col-auto">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-full-rvb"></use></svg></a>
        <a class="brand-dates" data-aos="fade-left" data-aos-delay="800" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-dates-rvb"></use></svg></a>
      </div>
      <div class="col-auto">
        <div class="row align-items-center">
          <div class="col-auto d-none d-sm-flex d-lg-none">
            <a href="<?= esc_url(home_url('/product-category/obtenir-un-pass/')); ?>" class="btn"><?php _e('Obtenir un pass','sppsante'); ?></a>
          </div>
          <div class="col-auto">
            <a data-active="header.banner" class="color-gray-dark"><svg class="svg-30"><use xlink:href="#svg-burger"></use></svg></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<header class="banner">
  <div class="header-actions row-lg d-lg-flex justify-content-lg-end" <?php if(is_front_page()) { echo ' data-aos="fade-down" data-aos-delay="1200"'; } ?>>
    <div class="d-lg-none mt-15 mb-30">
      <a data-active="header.banner" class="color-gray-dark"><svg class="svg-25"><use xlink:href="#svg-cross"></use></svg></a>
    </div>
    <div class="header-actions-links col-lg-auto order-lg-2">
      <a href="<?php the_permalink(26); ?>" class="color-rose"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-infos"></use></svg> <span class="d-none d-lg-inline small"><?php _e('Infos pratiques','sppsante'); ?></span></a>
      <a href="<?php if(is_user_logged_in()) echo home_url('/mon-compte/'); else echo '#sidebar-login'; ?>" class="color-violet active-sidebar"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-cadenas"></use></svg> <span class="d-none d-lg-inline small"><?php _e('Espace exposants','sppsante'); ?></span></a>
      <a href="<?php the_permalink(718); ?>" class="color-violet"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-search"></use></svg> <span class="d-none d-lg-inline small"><?php _e('Rechercher','sppsante'); ?></span></a>
    </div>
    <div class="header-actions-follow col-lg-auto order-lg-1">
      <span class="color-violet d-none d-lg-inline small"><?php _e('Suivez-nous','sppsante'); ?></span>
      <?php if(get_field('facebook','options')) :?>
        <a href="<?php the_field('facebook','options'); ?>" target="_blank" class="color-violet"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-facebook"></use></svg></a>
      <?php endif; ?>
      <?php if(get_field('twitter','options')) :?>
        <a href="<?php the_field('twitter','options'); ?>" target="_blank" class="color-violet"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-twitter"></use></svg></a>
      <?php endif; ?>
      <?php if(get_field('linkedin','options')) :?>
        <a href="<?php the_field('linkedin','options'); ?>" target="_blank" class="color-violet"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-linkedin"></use></svg></a>
      <?php endif; ?>
      <?php if(get_field('youtube','options')) :?>
        <a href="<?php the_field('youtube','options'); ?>" target="_blank" class="color-violet"><svg class="svg-30 svg-lg-25"><use xlink:href="#svg-youtube"></use></svg></a>
      <?php endif; ?>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row justify-content-between align-items-center">

      <div class="col-auto pr-0 brand-logo">
        <?php if(is_front_page()) : ?>
          <a id="logotypespp" class="brand" href="<?= esc_url(home_url('/')); ?>"></a>
          <a class="brand-dates" data-aos="fade-left" data-aos-delay="800" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-dates-rvb"></use></svg></a>
        <?php else : ?>
          <a class="brand" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-full-rvb"></use></svg></a>
          <a class="brand-dates" href="<?= esc_url(home_url('/')); ?>"><svg><use xlink:href="#svg-logo-dates-rvb"></use></svg></a>
        <?php endif; ?>
      </div>

      <div class="col-auto pl-0">
        <nav class="nav-primary" <?php if(is_front_page()) { echo ' data-aos="fade-right" data-aos-delay="800"'; } ?>>
          <ul id="menu-menu" class="nav">
            <li class="d-lg-none"><a href="<?= esc_url(home_url('/product-category/obtenir-un-pass/')); ?>" class="btn"><?php _e('Obtenir un pass','sppsante'); ?></a></li>
            <?php 
            $items = [8,50,53,55,18,57];

            foreach ($items as $item) :
              $childrens = get_pages(
                array( 'child_of' => $item, 
                'sort_order' => 'asc',
                'sort_column' => 'menu_order'
                )
              );
              
              $submenu = '';
              $inactivemenu = get_field('inactive', $item);
              $currentmenu = false;

              if($childrens) :

                $submenu .= '<ul class="sub-menu">';

                foreach ($childrens as $children) :

                  $submenu .= '<li class="menu-item"><a class="d-inline-block mb-0 mb-lg-20';
                  if(get_the_ID() == $children->ID || ( (is_single() || is_home() ) && $children->ID == get_option( 'page_for_posts' ))) { $submenu .= ' active'; $currentmenu = true; }
                  if(in_array($children->ID, array(64,74,226))) $submenu .= ' color-rose';
                  if(get_field('inactive', $children->ID) == true ) { $submenu .= ' inactive'; }
                  $submenu .= '"';
                  if(in_array($children->ID, array(64,74,226))) $submenu .= ' target="_blank"';
                  $submenu .= ' href="'.get_permalink($children->ID).'">'.get_the_title($children->ID).'</a>
                  </li>';

                  if(get_field('inactive', $children->ID) == false ) { $inactivemenu = false; }

                endforeach;

                $submenu .= '</ul>';
                
              endif;

              echo '<li class="menu-item';
              if($inactivemenu == false && $childrens) { echo ' menu-item-has-children'; }
              if($currentmenu == true || get_the_ID() == $item) { echo ' active'; }
              if($inactivemenu == true) { echo ' inactive'; }
              echo '"><a href="'.get_permalink($item).'">'.get_the_title($item).'</a>';
              if($inactivemenu == false && $submenu) { echo $submenu; }
              echo '</li>';

            endforeach;
            ?>
            <li class="menu-item d-none d-lg-block"><a href="<?= get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" target="_blank" class="btn"><?php _e('Obtenir un pass','sppsante'); ?></a></li>
          </ul>
        </nav>
      </div>

    </div>
  </div>

</header>
