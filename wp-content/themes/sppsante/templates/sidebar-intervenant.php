<div class="sidebar-persona" id="sidebar-<?php the_ID(); ?>">
  <a href="#" class="sidebar-persona-close"><svg class="svg-15 svg-sm-25 fill-white"><use xlink:href="#svg-cross"></use></svg></a>
  <?php the_post_thumbnail( 'square' ); ?>
  <?php the_title( '<h2 class="mt-15 mt-lg-30 color-rose">', '</h2>' ); ?>
  <?php if( get_field('intervenant_parcours') ): ?><p class="color-white"><strong><?php the_field( 'intervenant_parcours' ); ?></strong></p><?php endif; ?>
  <div class="sidebar-persona-content small color-white mt-30"><?php the_content(); ?></div>

  <?php
  //args global
  $argsProgrammes = array(
    'posts_per_page' => -1,
    'post_type'		    => 'programmes',
    'post_status'     => 'publish',
    'order'           => 'ASC',
    'orderby'         => 'meta_value',
    'meta_key'        => 'heure_de_debut',
    'meta_query' => array(
      array(
        'key'     => 'intervenant',
        'value'   => get_the_ID(),
        'compare' => '=',
      ),
    )
  );

  //Get posts
  $getProgrammes = get_posts($argsProgrammes);
  ?>

  <?php if ( $getProgrammes ) : ?>
    <h4 class="color-violet mt-40"><?php _e('Cet intervenant présente :','sppsante'); ?></h4>
    <?php foreach ($getProgrammes as $indexProgramme => $theProgramme) : ?>
      <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
      <a href="#sidebar-<?php echo $theProgramme->ID; ?>" class="active-sidebar d-flex align-items-end font-weight-bold color-white small py-15 border-bottom">
        <?php the_title(); ?><br>
        <?php if(get_field('date') == 'jeudi') _e('Jeudi 31 Janvier','sppsante'); elseif (get_field('date') == 'vendredi') _e('Vendredi 01 Février','sppsante'); ?> | <?php echo get_field('heure_de_debut').' - '.get_field('heure_de_fin'); ?>
      </a>
    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>
</div>