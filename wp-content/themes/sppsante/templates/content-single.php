<a href="<?php the_permalink(); ?>" <?php post_class('d-block'); ?>>
  <header>
    <?php if($count && $count %2) : ?>
      <?php the_post_thumbnail( 'medium', ['class' => 'w-100'] ); ?>
    <?php else: ?>
      <?php the_post_thumbnail( 'square', ['class' => 'w-100'] ); ?>
    <?php endif; ?>
  </header>
  <div class="entry-content small">
    <time class="small" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date('d.m - Y'); ?></time>
    <h3 class="color-violet"><?php the_title(); ?></h3>
    <?php echo get_the_excerpt(); ?>
  </div>
</a>