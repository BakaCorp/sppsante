<div class="mt-50 mt-sm-60 mt-lg-100" data-aos="fade-up">
  <?php if( $element['titre'] ): ?>
    <div class="col-12 col-xl-10 mx-xl-auto">
      <h2 class="mb-20 mb-sm-30"><?php echo $element['titre']; ?></h2>
    </div>
  <?php endif; ?>
  <?php if( $element['fichier'] ): ?>
    <div class="col-12 col-xl-10 mx-xl-auto">
      <a href="<?php echo $element['fichier']; ?>" target="_blank" class="btn">Télécharger</a>
    </div>
  <?php endif; ?>
</div>