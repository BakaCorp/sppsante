<?php
$getIntervenants = get_posts(array(
  'numberposts'	=> 6,
  'post_type'		=> 'intervenants',
  'post_status'     => 'publish',
  'order'           => 'ASC',
  'orderby'         => 'menu_order',
  'meta_query' => array(
    array(
      'key'     => 'intervenant_cat',
      'value'   => 'comite',
      'compare' => 'LIKE',
    )
  )
));
?>

<?php if ( $getIntervenants ) : ?>
  <div class="row">
    
    <div class="col-12 titre-decale">
      <h1 class="color-violet" data-aos="fade-down"><?php _e('Comité d\'éthique','sppsante'); ?></h1>
      <div class="slider-orateurs" data-aos="fade-up">
      
        <?php foreach ($getIntervenants as $indexIntervenant => $theIntervenant) : ?>
          <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>
          <a href="#sidebar-<?php the_ID(); ?>" class="d-block persona <?php if(get_field('sexe') == 'Femme') echo 'persona-femme'; ?> active-sidebar">
            <?php the_post_thumbnail( 'square', ['class' => 'w-100'] ); ?>
            <?php the_title( '<h3 class="mt-15 mt-lg-30">', '</h3>' ); ?>
            <?php if( get_field('intervenant_parcours') ): ?><p><?php the_field( 'intervenant_parcours' ); ?></p><?php endif; ?>
          </a>
        <?php endforeach; ?>
        <a href="<?php the_permalink(10); ?>" class="d-block persona">
          <img src="<?php echo get_template_directory_uri() . '/dist/images/violet.jpg'; ?>" class="w-100">
          <h3 class="mt-15 mt-lg-30"><?php _e( 'Tout le comité', 'sppsante' ); ?> <svg class="svg-20"><use xlink:href="#svg-arrow-left"></use></svg></h3>
        </a>

      </div>
    </div>
  </div>
  <?php wp_reset_postdata(); ?>
  
  <?php foreach ($getIntervenants as $theIntervenant) : ?>
    <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>
    <?php include TEMPLATEPATH . '/templates/sidebar-intervenant.php'; ?>

    <?php if ( $getProgrammes ) : ?>
      <?php foreach ($getProgrammes as $indexProgramme => $theProgramme) : ?>
        <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
        <?php include TEMPLATEPATH . '/templates/sidebar-programme.php'; ?>      
      <?php endforeach; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>

  <div class="back-sidebar"></div>
<?php endif; ?>