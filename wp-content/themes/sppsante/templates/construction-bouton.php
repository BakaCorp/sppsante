<div class="mt-50 mt-sm-60 mt-lg-100" data-aos="fade">
  <div class="col-12 col-xl-10 mx-xl-auto">
    <a href="<?php if( $element['lien'] ) { echo get_permalink( $element['lien'][0] ); } ?>" class="btn <?php if( $element['lien'] == false || get_field('inactive', $element['lien'][0]) == true ) { echo 'inactive'; } ?>"><?php echo $element['texte']; ?></a>
  </div>
</div>