<a href="<?php the_permalink(); ?>" <?php post_class('d-block border-bottom pb-30 mb-30'); ?>>
  <div class="entry-content small">
    <time class="small" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date('d.m - Y'); ?></time>
    <h3 class="color-violet"><?php the_title(); ?></h3>
    <?php echo get_the_excerpt(); ?>
  </div>
</a>