<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <?php 
  if(get_the_post_thumbnail()) {
    $page_header_xs = get_the_post_thumbnail_url( get_the_ID(), 'page-header-xs' );
    $page_header_sm = get_the_post_thumbnail_url( get_the_ID(), 'page-header-sm' );
    $page_header_xl = get_the_post_thumbnail_url( get_the_ID(), 'page-header-xl' );
    ?>
    <picture>
      <source media="(max-width: 767px)" srcset="<?php echo esc_attr( $page_header_xs ); ?>">
      <source media="(max-width: 991px)" srcset="<?php echo esc_attr( $page_header_sm ); ?>">
      <?php the_post_thumbnail( 'page-header-xl', ['class' => 'w-100']  ); ?>
    </picture>
    <?php
  } else {
    echo '<div class="page-header-nopicture"></div>';
  }
  ?>
</div>
