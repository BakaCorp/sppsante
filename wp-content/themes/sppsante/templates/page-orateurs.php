<!-- CONTENT PAGE ORATEURS -->

<?php
//detect the set number of posts per page
$ppp = get_option('posts_per_page');

// args global
$argsIntervenants = array(
  'numberposts' => -1,
  'post_type'		=> 'intervenants',
  'post_status'     => 'publish',
  'order'           => 'ASC',
  'orderby'         => 'menu_order',
  'meta_query' => array(
    array(
      'key'     => 'intervenant_cat',
      'value'   => 'orateur',
      'compare' => 'LIKE',
    )
  )
);

//if category
if($_GET['cat']) $argsIntervenants['category'] = $_GET['cat'];

//Get posts
$getIntervenants = get_posts($argsIntervenants);
?>

<?php if ( $getIntervenants ) : ?>

  <div class="col-12 col-xl-10 mx-xl-auto">
    <div class="row">
      <div class="col-12 py-30">
        <div class="dropdown-group">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php
            if($_GET['cat']) {
              echo get_cat_name( $_GET['cat'] );
            } else {
              _e('Toutes les catégories','sppsante');
            }
            ?>
          </a>
          <div class="dropdown-menu">
            <?php
            $categories = get_categories(array(
              'orderby' => 'name',
              'order' => 'ASC', 
              'hide_empty' => true, 
              'exclude' => '1' 
            ));
            ?>
            <a class="dropdown-item" href="<?php the_permalink(); ?>"><?php _e('Toutes les catégories','sppsante'); ?></a>
            <?php foreach( $categories as $category ) : ?>
              <a class="dropdown-item" href="?cat=<?php echo $category->term_id; ?>"><?php echo $category->name; ?></a>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <?php foreach ($getIntervenants as $indexIntervenant => $theIntervenant) : ?>
        <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>

        <a href="#sidebar-<?php the_ID(); ?>" class="persona <?php if(get_field('sexe') == 'Femme') echo 'persona-femme'; ?> active-sidebar col-12 col-sm-6 col-lg-4 mb-30 mb-sm-40 mb-lg-60" data-aos="fade-left">
          <div>
            <?php the_post_thumbnail( 'square', ['class' => 'w-100'] ); ?>
            <?php the_title( '<h3 class="mt-15 mt-lg-30">', '</h3>' ); ?>
            <?php if( get_field('intervenant_parcours') ): ?><p><?php the_field( 'intervenant_parcours' ); ?></p><?php endif; ?>
          </div>
        </a>
        
      <?php endforeach; ?>
    </div>
    
  </div>
  <?php wp_reset_postdata(); ?>


  <?php foreach ($getIntervenants as $theIntervenant) : ?>
    <?php $post = get_post( $theIntervenant->ID ); setup_postdata( $post ); ?>
    <?php include TEMPLATEPATH . '/templates/sidebar-intervenant.php'; ?>

    <?php if ( $getProgrammes ) : ?>
      <?php foreach ($getProgrammes as $indexProgramme => $theProgramme) : ?>
        <?php $post = get_post( $theProgramme->ID ); setup_postdata( $post ); ?>
        <?php include TEMPLATEPATH . '/templates/sidebar-programme.php'; ?>      
      <?php endforeach; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>

  <div class="back-sidebar"></div>
<?php endif; ?>