<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<?php while (have_posts()) : the_post(); ?>

  <div id="slider-homepage" class="d-lg-flex justify-content-lg-between align-items-center" data-aos="fade-right" data-aos-delay="800">
    <a class="slider-prev d-flex justify-content-center align-items-center color-white">
      <svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-right"></use></svg>
    </a>
    <a class="slider-next d-flex justify-content-center align-items-center color-white">
      <svg class="slider-circle"><circle r="30" cx="32" cy="32"></circle></svg>
      <svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-left"></use></svg>
    </a>
    <div class="slider-content">
      <?php $slides = get_field('slides'); ?>
      <?php foreach ($slides as $order => $slide) : ?>
        <div class="slider-slide d-flex align-items-center <?php if($order == 0) { echo 'active'; } ?>" data-slide="<?php echo $order+1; ?>" style="background-image: url(<?php echo $slide['image']; ?>);">
          <div>
            <?php 
            $accroche = $slide['titre'];
            $arrayAccroche = preg_split('/<br[^>]*>/i', $accroche);
            for ($i=0; $i < count($arrayAccroche) ; $i++) { 
              echo '<div class="fade-left">'.$arrayAccroche[$i].'</div>';
            }
            ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="slider-slogan">
      <h2 class="color-white" data-aos="fade-left" data-aos-delay="1600"><?php the_field('slogan','options'); ?></h2>
      <h3 class="color-rose" data-aos="fade-left" data-aos-delay="1900"><?php the_field('slogan_suite','options'); ?></h3>
    </div>
    <video autoplay loop muted playsinline>
      <source src="<?php echo get_template_directory_uri(); ?>/dist/custom/adn.mp4" type="video/mp4">
    </video>
  </div>

  
  <?php if ( get_field('encart_pub_haut','options') ) : ?>
    <div class="container-fluid py-20 py-lg-30 text-center background-gray-light">
      <?php if ( get_field('lien_encart_pub_haut','options') ) : ?>
        <a href="<?php the_field('lien_encart_pub_haut','options'); ?>" target="_blank"><img src="<?php the_field('encart_pub_haut','options'); ?>" class="mw-100"></a>
      <?php else: ?>
        <img src="<?php the_field('encart_pub_haut','options'); ?>" class="mw-100">
      <?php endif; ?>
    </div><!-- /.container-fluid -->
  <?php endif; ?>


  <div class="container-fluid py-50 py-lg-100 background-filigrane">
    <div class="container">
      <div class="row align-items-center">

        <div class="d-none d-sm-block background-white col-sm-5 col-lg-6" data-aos="fade-right">
          <?php the_post_thumbnail( 'large', ['class' => 'w-100']  ); ?>
        </div>

        <div class="col-12 col-sm-6 col-lg-5 offset-sm-1 homepage-prevention" data-aos="fade-left">
          <?php the_content(); ?>
        </div>

      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /.container-fluid -->

  <div class="container-fluid background-gray-light py-50 py-lg-100">
    <div class="container">
      <?php include TEMPLATEPATH . '/templates/frontpage-comite.php'; ?>
    </div><!-- /.container -->
  </div><!-- /.container-fluid -->

  <div class="container-fluid py-50 py-lg-100">
    <div class="container">
      <?php include TEMPLATEPATH . '/templates/frontpage-actualites.php'; ?>
    </div><!-- /.container -->
  </div><!-- /.container-fluid -->

<div class="container-fluid background-gray-light py-50 py-lg-100">
  <div class="container">
    <?php include TEMPLATEPATH . '/templates/frontpage-partenaires.php'; ?>
  </div><!-- /.container -->
</div><!-- /.container-fluid -->

<?php endwhile; ?>