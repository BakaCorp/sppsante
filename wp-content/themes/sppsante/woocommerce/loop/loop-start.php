<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="text-center mb-20">
	<h2 class="color-rose"><?= _e('Inscription au salon SPPSanté 2019','sppsante'); ?></h2>
	<p><?= _e('Si vous souhaitez inscrire <span class="color-rose">plusieurs personnes</span> ou pour <span class="color-rose">les groupes</span>, veuillez ','sppsante'); ?><a href="<?php echo get_permalink(28); ?>" class="color-rose"><?= _e('nous contacter','sppsante'); ?></a></p>
</div>

<div class="products row">
	<div class="col-12">
		<h3><?php _e('Choisissez un pass','sppsante'); ?></h3>
	</div>