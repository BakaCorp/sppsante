<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

//wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<div class="text-center mb-50">
	<h2 class="color-rose"><?= _e('Inscription au salon SPPSanté 2019','sppsante'); ?></h2>
	<p><?= _e('Si vous souhaitez inscrire <span class="color-rose">plusieurs personnes</span> ou pour <span class="color-rose">les groupes</span>, veuillez ','sppsante'); ?><a href="<?php echo get_permalink(28); ?>" class="color-rose"><?= _e('nous contacter','sppsante'); ?></a></p>
</div>

<div class="row mb-50">
	<div class="col-12 col-sm-4 text-center"><a href="<?= get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="h4 color-violet"><?= _e('1. Choix du Pass','sppsante') ?></a></div>
	<div class="col-12 col-sm-4 text-center"><a href="<?= get_permalink( woocommerce_get_page_id( 'cart' ) ); ?>" class="h4 color-rose"><?= _e('2. Validation','sppsante') ?></a></div>
	<div class="col-12 col-sm-4 text-center"><span class="h4 color-gray"><?= _e('3. Coordonnées et Paiement','sppsante') ?></span></div>
</div>

<div class="product text-center p-30">
	<?php
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

		_e('Vous êtes sur le point de vous procurer le pass ','sppsante');
		echo '<span class="color-rose">'.$_product->get_name().'</span> ';
		echo '<br>';
		_e('pour un total HT de ','sppsante');
		wc_cart_totals_subtotal_html();
	}
	?>
</div>

<div class="py-30 text-right">
	<a href="<?= get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="btn btn-light mr-10"><?= _e( 'Retour', 'sppsante' ); ?></a>
	<a href="<?= get_permalink( woocommerce_get_page_id( 'checkout' ) ); ?>" class="btn"><?= _e( 'Continuer et payer', 'sppsante' ); ?></a>
</div>