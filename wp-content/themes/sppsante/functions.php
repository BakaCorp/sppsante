<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',     // Scripts and stylesheets
  'lib/extras.php',     // Custom functions
  'lib/setup.php',      // Theme setup
  'lib/titles.php',     // Page titles
  'lib/wrapper.php',    // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/custom.php',     // Custom this Theme
  'lib/post-types.php'  // Custom Post Types
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

/*
 * Custom Navigation
 */
function sppagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default queries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => '<svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-right"></use></svg>',
    'next_text'       => '<svg class="svg-15 svg-sm-30"><use xlink:href="#svg-arrow-left"></use></svg>',
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo $paginate_links;
    echo "</nav>";
  }
}
add_filter('sppagination', __NAMESPACE__ . '\\sppagination');

/**
* is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
*
* @access public
* @return bool
*/
function is_realy_woocommerce_page () {
  if( function_exists ( "is_woocommerce" ) && is_woocommerce()){
      return true;
  }
  $woocommerce_keys = array ( "woocommerce_shop_page_id" ,
      "woocommerce_terms_page_id" ,
      "woocommerce_cart_page_id" ,
      "woocommerce_checkout_page_id" ,
      "woocommerce_pay_page_id" ,
      "woocommerce_thanks_page_id" ,
      //"woocommerce_myaccount_page_id" ,
      "woocommerce_edit_address_page_id" ,
      "woocommerce_view_order_page_id" ,
      "woocommerce_change_password_page_id" ,
      "woocommerce_logout_page_id" ,
      "woocommerce_lost_password_page_id" ) ;

  foreach ( $woocommerce_keys as $wc_page_id ) {
      if ( get_the_ID() && get_the_ID() == get_option ( $wc_page_id , 0 ) ) {
        return true ;
      }
  }
  return false;
}


function redirect_to_checkout() {
  global $woocommerce;
  $checkout_url = $woocommerce->cart->get_checkout_url();
  return $checkout_url;
}
add_filter ('add_to_cart_redirect', __NAMESPACE__ . '\\redirect_to_checkout');


function cart_description( $name, $cart_item, $cart_item_key ) {
    // Get the corresponding WC_Product
    $product_item = $cart_item['data'];

    if(!empty($product_item)) {
        // WC 3+ compatibility
        $description = $product_item->get_description();
        return '<h4 class="strong color-rose">' . $name . '</h4><p>' . $description . '</p>';
    } else {
        return $name;
    }
}
add_filter( 'woocommerce_cart_item_name', __NAMESPACE__ . '\\cart_description', 20, 3);


function rfvc_update_order_status( $order_status, $order_id ) {
  $order = new WC_Order( $order_id );
  if ( 'processing' == $order_status && ( 'on-hold' == $order->status || 'pending' == $order->status || 'failed' == $order->status ) ) {
    return 'completed';
  }
  return $order_status;
}
add_filter( 'woocommerce_payment_complete_order_status', __NAMESPACE__ . '\\rfvc_update_order_status', 10, 2 );